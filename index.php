﻿<?php
	$title = "Ξενοδοχείο Elea Mare στην Ελιά Μονεμβασίας Λακωνίας";
	$discription = "Το ξενοδοχείο Elea Mare βρίσκεται στην Ελιά Μονεμβασίας Λακωνίας. Η θέση του είναι ιδανική, με καταπληκτική θέα στη θάλασσα, απολαυστικό ηλιοβασίλεμα και άμεση πρόσβαση στην παραλία. Απέχει 400 μέτρα από το κέντρο του οικισμού και βρίσκεται σε θέση που προσφέρεται για περιήγηση σε όλες τις όμορφες και ιστορικές περιοχές του Νομού Λακωνίας.";
	include('header.php');
?>

	<body id="homepage">

	<!-- Google Analytics -->
	
	<?php include_once("analyticstracking.php") ?>

	<!-- PRELOADER -->

	<div class="modal">
		<div class="pre-logo"></div>
	</div>

	<!-- MOBILE MENU - VIEPORT < 640PX -->

	<?php include('navigation_mobile.php');?>

	<header id="header">
		
		<!-- SLIDER -->

		<div id="slider" class="nivoSlider">
	    <img src="img/slider/slider.jpg" alt="Ξενοδοχείο Elea Mare - Ελιά Μονεμβασίας Λακωνίας" />
	    <img src="img/slider/slider2.jpg" alt="Ξενοδοχείο Elea Mare - Ελιά Μονεμβασίας Λακωνίας" />
	    <img src="img/slider/slider3.jpg" alt="Ξενοδοχείο Elea Mare - Ελιά Μονεμβασίας Λακωνίας" />
	    <img src="img/slider/slider4.jpg" alt="Ξενοδοχείο Elea Mare - Ελιά Μονεμβασίας Λακωνίας" />
	    <img src="img/slider/slider5.jpg" alt="Ξενοδοχείο Elea Mare - Ελιά Μονεμβασίας Λακωνίας" />
	    <img src="img/slider/slider6.jpg" alt="Ξενοδοχείο Elea Mare - Ελιά Μονεμβασίας Λακωνίας" />
  	</div>

  <!-- INCLUDE NAVIGATION & BOOK NOW -->
		
	<?php include_once('navigation.php');?>

	</header>
		
	<section id="main" class="container">
		
		<div class="mainbar grid-5">
			
			<div class="main-text">
				<h1>Καλωσήρθατε στο Elea Mare,</h1>
				<h2>στην καρδιά της Λακωνίας...</h2>
				<p>
					Στην Ελιά, ένα γραφικό ψαροχώρι που ανήκει στο Δήμο Μονεμβασίας, βρίσκεται το ξενοδοχείο Elea Mare σε ήσυχο και φιλικό περιβάλλον. Η τοποθεσία του είναι ιδανική με καταπληκτική θέα στη θάλασσα, απολαυστικό ηλιοβασίλεμα και άμεση πρόσβαση στην παραλία.
				</p>
				<p>
					Απέχει 400 μέτρα από το κέντρο του οικισμού και βρίσκεται σε κομβική θέση, η οποία προσφέρεται για περιήγηση σε όλες τις όμορφες και ιστορικές περιοχές του Νομού Λακωνίας.
				</p>
				<p>
					Οι επισκέπτες μπορούν να εξερευνήσουν το μεσαιωνικό κάστρο της Μονεμβασιάς, που απέχει 30χλμ. από το ξενοδοχείο. Επίσης, τα Σπήλαια του Δυρού, η οχυρωμένη πόλη του Μυστρά και ο φημισμένος Σίμος της Ελαφονήσου αποτελούν βασικές ημερήσιες εκδρομές με ορμητήριο το Elea Mare.
				</p>
			</div>

			<div class="home-gallery">
				<ul>
					<li class="img-1"><img src="img/img1.jpg" alt="Διακοπές στην Ελιά Μονεμβασίας Λακωνίας" /></li>
					<li class="img-2"><img src="img/img2.jpg" alt="Θέα στη θάλασσα" /></li>
					<li class="img-3"><img src="img/img3.jpg" alt="Ξενοδοχείο με θέα στη θάλασσα" /></li>
				</ul>
			</div>

		</div>

		<div class="sidebar grid-1">
			<div class="weather">
				<iframe scrolling="no" frameborder="0" allowtransparency="true" src="http://www.weather.gr/gr/gr/widgets/weather_w2.aspx?p=18036" style="width: 120px; height: 410px"></iframe><a target="blank" style="color: #999999; text-align: center; display: block; font: 10px/10px Arial,san-serif; text-decoration: none;" href="http://www.weather.gr">καιρός weather.gr</a>
			</div>
			<a href="elia_monemvasias_lakonias.php#formanchor" class="social-button">Πώς θα έρθετε</a>
		</div>

	</section>
	
	<!-- INCLUDE FOOTER -->

	<?php include_once('footer.php');?>
		
	</body>
</html>
