<?php

	$title = "Ξενοδοχείο Elea Mare:: διαμονή";

	$discription = "Φωτογραφίες και λεπτομέρειες σχετικά με την διαμονή στο Elea Mare Hotel.";

	include('header.php');

?>



	<body id="accommodation">



	<!-- Google Analytics -->



	<?php include_once("analyticstracking.php") ?>



	<!-- MOBILE MENU - VIEPORT < 640PX -->



	<?php include('navigation_mobile.php');?>



  <header id="header">



		<!-- MAIN BACKGROUND -->



		<div id="slider" class="nivoSlider">

	    <img src="img/main-backgrounds/accommodation.jpg" alt="Διαμονή στο Elea Mare" />

  	</div>



  	<!-- INCLUDE NAVIGATION & BOOK NOW -->



		<?php include_once('navigation.php');?>



	</header>



		<section id="main" class="container">



			<div class="mainbar grid-4">

				<div class="main-text">

					<h1>Διαμονή - Book direct for Exclusive Offers</h1>

					<p>

						Το ξενοδοχείο διαθέτει 22 studios. Κάθε ένα από αυτά είναι ευρύχωρο με κλιματισμό, Wi-Fi, ατομική κουζίνα, ψυγείο, τηλεόραση, στεγνωτήρα μαλλιών, τηλέφωνο και μπάνιο με ντους.<br><span style="font-style:italic">* Τα κατοικίδια δεν επιτρέπονται</span>.

					</p>

					<p class="checkin">Check in: 14:00 – 21:00 / Check out: 7:00 – 12:00.</p>

				</div>

			</div>



			<div class="sidebar grid-2">

				<h1>Παροχές</h1>

				<ul class="services">

					<li>Δωρεάν Wi-Fi</li>

					<li>Πρωινό Continental (8:00 - 12:00)</li>

					<li>Xώρος στάθμευσης</li>

					<li>Ενοικίαση αυτοκινήτου</li>

				</ul>

			</div>



			<div class="categories grid-full">

				<h1>Τύποι Δωματίων</h1>

				<p class="grid-full"> * Κάντε click στις φωτογραφίες για να δείτε περισσότερες</p>


				<h2>Δίκλινο</h2>

				<div class="type-categories grid-2">

					<a href="img/accommodation/new/double-studio-partial-sea-view-1.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View"><img src="img/accommodation/new/double-studio-partial-sea-view-1.jpg" alt="Δίdouble-studio-partial-sea-view"></a>
					<a href="img/accommodation/new/double-studio-partial-sea-view-2.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View" class="hidden"></a>
					<a href="img/accommodation/new/double-studio-partial-sea-view-3.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View" class="hidden"></a>
					<a href="img/accommodation/new/double-studio-partial-sea-view-4.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View" class="hidden"></a>
					<h3>Δίκλινο Στούντιο με Μερική Θέα στη Θάλασσα
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 2 Μονά Κρεβάτια<br>
								- 35 τετραγωνικά μέτρα<br>
								- Ισόγειο
							</span>
						</span>
					</h3>


				</div>


				<div class="type-categories grid-2">

					<a href="img/accommodation/new/double-seafront-1.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront"><img src="img/accommodation/new/double-seafront-1.jpg" alt="double-seafront"></a>
					<a href="img/accommodation/new/double-seafront-2.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront" class="hidden"></a>
					<a href="img/accommodation/new/double-seafront-3.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront" class="hidden"></a>
					<a href="img/accommodation/new/double-seafront-4.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront" class="hidden"></a>
 					<h3>Standard Δίκλινο Στούντιο με Θέα στη Θάλασσα
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 2 Μονά Κρεβάτια<br>
								- 35 τετραγωνικά μέτρα<br>
								- Ισόγειο ή πάνω όροφος ανάλογα τη διαθεσιμότητα
							</span>
						</span>
					</h3>

				</div>



				<div class="type-categories grid-2">

					<a href="img/accommodation/new/superior-double-1.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront"><img src="img/accommodation/new/superior-double-1.jpg" alt="superior-double"></a>
					<a href="img/accommodation/new/superior-double-2.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront" class="hidden"></a>
					<a href="img/accommodation/new/superior-double-3.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront" class="hidden"></a>
					<a href="img/accommodation/new/superior-double-4.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront" class="hidden"></a>
					<h3>Superior Δίκλινο Στούντιο με Θέα στη Θάλασσα
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 1 King Size κρεβάτι Coco-Mat (2μΧ2μ)<br>
								- 35 τετραγωνικά μέτρα<br>
								- Πάνω Όροφος<br>
								- Χρηματοκιβώτιο μεγέθους φορητού υπολογιστή
							</span>
						</span>
					</h3>

				</div>



			</div>

			<div class="categories grid-full">

				<h2>Τρίκλινο</h2>

				<div class="type-categories grid-2">

					<a href="img/accommodation/new/triple-studio-1.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafront"><img src="img/accommodation/new/triple-studio-1.jpg" alt="triple-studio"></a>
					<a href="img/accommodation/new/triple-studio-2.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafront"></a>
					<a href="img/accommodation/new/triple-studio-3.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafront" class="hidden"></a>
					<a href="img/accommodation/new/triple-studio-4.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafront" class="hidden"></a>
					<h3>
						Standard Τρίκλινο Στούντιο με Θέα στη θάλασσα
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 3 μονά κρεβάτια<br>
								- 35 τετραγωνικά μέτρα<br>
								- Ισόγειο ή πάνω όροφος ανάλογα τη διαθεσιμότητα
							</span>
						</span>
					</h3>


				</div>


				<div class="type-categories grid-2">

					<a href="img/accommodation/new/superior-triple-1.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront"><img src="img/accommodation/new/superior-triple-1.jpg" alt="superior-triple"></a>
					<a href="img/accommodation/new/superior-triple-2.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront" class="hidden"></a>
					<a href="img/accommodation/new/superior-triple-3.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront" class="hidden"></a>
					<a href="img/accommodation/new/superior-triple-4.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront" class="hidden"></a>
 					<h3>
						Superior Τρίκλινο Στούντιο με Θέα στη θάλασσα
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 1 King Size κρεβάτι Coco-Mat (2μΧ2μ) + 1 Μονό Κρεβάτι<br>
								- 40 τετραγωνικά μέτρα<br>
								- Πάνω Όροφος<br>
								- Χρηματοκιβώτιο μεγέθους φορητού υπολογιστή
							</span>
						</span>
					</h3>

				</div>


			</div>

			<div class="categories grid-full">

				<h2>Τετράκλινο Διαμέρισμα Δύο Δωματίων</h2>


				<div class="type-categories grid-2">

					<a href="img/accommodation/new/standard-two-bedroom-apartment-1.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront"><img src="img/accommodation/new/standard-two-bedroom-apartment-1.jpg" alt="standard-two-bedroom-apartment"></a>
					<a href="img/accommodation/new/standard-two-bedroom-apartment-2.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront" class="hidden"></a>
					<a href="img/accommodation/new/standard-two-bedroom-apartment-3.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront" class="hidden"></a>
					<a href="img/accommodation/new/standard-two-bedroom-apartment-4.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront" class="hidden"></a>
					<h3>
						Standard Διαμέρισμα Δύο Δωματίων με Θέα στη Θάλασσα
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 4 μονά κρεβάτια<br>
								- 50 τετραγωνικά μέτρα<br>
								- Ισόγειο
							</span>
						</span>
					</h3>


				</div>


				<div class="type-categories grid-2">

					<a href="img/accommodation/new/superior-two-bedroom-apartment-1.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"><img src="img/accommodation/new/superior-two-bedroom-apartment-1.jpg" alt="superior-two-bedroom-apartment"></a>
					<a href="img/accommodation/new/superior-two-bedroom-apartment-2.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"></a>
					<a href="img/accommodation/new/superior-two-bedroom-apartment-3.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"></a>
					<a href="img/accommodation/new/superior-two-bedroom-apartment-4.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"></a>
 					<h3>
						Superior Διαμέρισμα Δύο Δωματίων με Θέα στη Θάλασσα
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 1 King Size κρεβάτι Coco-Mat (2μΧ2μ) + 2 Μονά Κρεβάτια<br>
								- 50 τετραγωνικά μέτρα<br>
								- Πάνω Όροφος<br>
								- Χρηματοκιβώτιο μεγέθους φορητού υπολογιστή
							</span>
						</span>
					</h3>

				</div>


			</div>

			<div class="categories grid-full">

				<h2>Acqua Σουίτα</h2>


				<div class="type-categories grid-2">

					<a href="img/accommodation/new/acqua-1.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)"><img src="img/accommodation/new/acqua-1.jpg" alt="acqua"></a>
					<a href="img/accommodation/new/acqua-2.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
					<a href="img/accommodation/new/acqua-3.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
					<a href="img/accommodation/new/acqua-4.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
					<a href="img/accommodation/new/acqua-5.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
					<a href="img/accommodation/new/acqua-6.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
					<a href="img/accommodation/new/acqua-7.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
					<a href="img/accommodation/new/acqua-8.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
					<h3>
						Acqua Σουίτα (Δύο Δίκλινα Συνδεδεμένα Στούντιο)
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 1 King Size κρεβάτι Coco-Mat (2μΧ2μ) + 2 Μονά Κρεβάτια<br>
								- 60 τετραγωνικά μέτρα<br>
								- Ισόγειο<br>
								- 1 Χρηματοκιβώτιο μεγέθους φορητού υπολογιστή<br>
								- 2 Τηλεοράσεις<br>
								- 2 Στεγνωτήρες Μαλλιών<br>
								- 2 Τουαλέτες<br>
								- 1 Ολόσωμος Καθρέφτης
							</span>
						</span>
					</h3>


				</div>

			</div>




		</section>



		<!-- INCLUDE FOOTER -->



		<?php include_once('footer.php');?>



	</body>

</html>
