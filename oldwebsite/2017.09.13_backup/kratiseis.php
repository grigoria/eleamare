﻿<?php
	$title = "Ξενοδοχείο Elea Mare:: έλεγχος διαθεσιμότητας";
	$discription = "Έλεγχος διαθεσιμότητας για το Ξενοδοχείο Elea Mare.";
	include('header.php');
?>

	<body id="contact-form">

		<!-- Google Analytics -->
	
		<?php include_once("analyticstracking.php") ?>

		<!-- MOBILE MENU - VIEPORT < 640PX -->

		<?php include('navigation_mobile.php');?>

		<header id="header">

			<!-- MAIN BACKGROUND -->

			<div id="slider" class="nivoSlider">
        <img src="img/main-backgrounds/contact.jpg" alt="1" />
      </div>
			
			<!-- INCLUDE NAVIGATION & BOOK NOW -->
		
			<?php include_once('navigation.php');?>
     
		</header>

		<section id="main" class="container">
			
			<div class="mainbar grid-4">
				<div class="main-text">
					<a name="form2anchor" id="form2anchor"></a>
					<h1>Έλεγχος διαθεσιμότητας - Book direct for Exclusive Offers</h1>
					<div class="contact-form grid-full">
						<?php
              if ($_GET['msg'] !="ok") {
            ?>

	            <form class="contact_form" method="post" action="check.php">
	                <div class="clear">
	                    <label for="checkinnew"> Άφιξη * :</label>
	                    <input type="text" id="checkinnew" name="checkinnew" value="<?php echo htmlspecialchars($_POST['checkin']) ?>" required/>
	                </div>
	                <div class="clear">
	                    <label for="checkoutnew">  Αναχώρηση * :</label>
	                    <input type="text" id="checkoutnew" name="checkoutnew" value="<?php echo htmlspecialchars($_POST['checkout']) ?>" required/>
	                </div>
	                <div class="clear">
	                    <label for="roomsnew"> Δωμάτια * :</label>
	                    <input type="text" id="roomsnew" name="roomsnew" value="<?php echo $_POST['rooms']; ?>" onkeypress="return isNumber(event)" required/>
	                </div>
	                <div class="clear">
	                    <label for="peoplenew"> Άτομα * :</label>
	                    <input type="text" id="peoplenew" name="peoplenew" value="<?php echo $_POST['adults']; ?>" onkeypress="return isNumber(event)" required/>
	                </div>
	                <div class="clear">
	                	<label for="bedsnew" style="padding-top: 18px;"> Τύπος έξτρα κρεβατιού :</label>
		                <select name="bedsnew" id="bedsnew">
		       							<option value="0">Kανένα</option>
		       							<option value="1">Έξτρα κρεβάτι (3-16 ετών)</option>
		        						<option value="2">Κούνια (1-2 ετών)</option>
		        					</select>
		        				</div>
	                <div class="clear">
	                    <label for="name"> Όνομα / Επώνυμο * :</label>
	                    <input type="text" id="name" name="name" placeholder="Όνομα / Επώνυμο" required/>
	                </div>
	                <div class="clear">
	                    <label for="country">  Χώρα / Πόλη:</label>
	                    <input type="text" id="country" name="country" placeholder="Χώρα / Πόλη" />
	                </div>
	                <div class="clear">
	                    <label for="phone"> Tηλέφωνο * :</label>
	                    <input type="text" id="phone" name="phone" placeholder="Τηλέφωνο"  onkeypress="return isNumber(event)" required/>
	                </div>
	                 <div class="clear">
	                    <label for="email"> E-mail * :</label>
	                    <input type="email" id="email" name="email" placeholder="E-mail" required/>
	                </div>
	                <div class="clear">
	                    <label for="message">  Μήνυμα * :</label>
	                    <textarea id="message" name="message" placeholder="Μήνυμα" required></textarea>
	                </div>
	                <div class="clear">
	                    <button type="submit" class="button2" value="αποστολή">
	                    <em>Αποστολή</em>
	                    </button>
	                    <input id="submitted" type="hidden" name="submitted" value="true" />
	                </div>
	            </form>

	            <?php
            		} else {
                	echo "<div id='sentmsg'>Το μήνυμά σας στάλθηκε με επιτυχία. Θα λάβετε άμεσα την προσφορά μας στο e-mail που δηλώσατε. Ευχαριστούμε πολύ.</div>";
            		}
              ?>
	        </div>
				</div>
			</div>

			<div class="sidebar grid-2">
				<h1>Στοιχεία επικοινωνίας</h1>
				<p>Ξενοδοχείο Elea Mare</p>
				<ul class="services">
					<li>Ελαία Μονεμβασίας, Λακωνία</li>
					<li>T.K.: 23052</li>
					<li>Tηλέφωνο: +30 27320 57540-1</li>
					<li>Fax: +30 27320 57551</li>
					<li>E-mail: eleamare@gmail.com</li>
				</ul>
				<p>Υποδοχή – Τηλεφωνικό Κέντρο: 7:00 – 21:00</p>
			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer.php');?>
		
	</body>
</html>
