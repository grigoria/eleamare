﻿<?php
	$title = "Elea Mare Hotel a Elea di Monemvasia, Lakonia";
	$discription = "A Elea, un pittoresco villaggio di pescatori che appartiene al comune di Monemvasia, si trova l’albergo Elea Mare in un ambiente tranquillo e accogliente. La sua posizione ? perfetta, con una meravigliosa vista mare, splendidi tramonti e accesso diretto alla spiaggia.
Si trova a 400 metri dal centro del villaggio ed ? situato in una posizione centrale, ideale per l'esplorazione e la visita di vari luoghi di interesse turistico e storico della Laconia. ";
	include('header_it.php');
?>

	<body id="homepage">

	<!-- Google Analytics -->
	
	<?php include_once("../analyticstracking.php") ?>

	<!-- PRELOADER -->

	<div class="modal">
		<div class="pre-logo"></div>
	</div>

	<!-- MOBILE MENU - VIEPORT < 640PX -->

	<?php include('navigation_mobile_it.php');?>

	<header id="header">
		
		<!-- SLIDER -->

		<div id="slider" class="nivoSlider">
	    <img src="../img/slider/slider.jpg" alt="1" />
	    <img src="../img/slider/slider2.jpg" alt="2" />
	    <img src="../img/slider/slider3.jpg" alt="3" />
	    <img src="../img/slider/slider4.jpg" alt="4" />
	    <img src="../img/slider/slider5.jpg" alt="5" />
	    <img src="../img/slider/slider6.jpg" alt="6" />
  	</div>

  <!-- INCLUDE NAVIGATION & BOOK NOW -->
		
	<?php include_once('navigation_it.php');?>

	</header>
		
	<section id="main" class="container">
		
		<div class="mainbar grid-5">
			
			<div class="main-text">
				<h1>Benvenuti a Elea Mare,</h1>
				<h2>nel cuore della Laconia...</h2>
				<p>
					A Elea, un pittoresco villaggio di pescatori che appartiene al comune di Monemvasia, si trova l’albergo Elea Mare in un ambiente tranquillo e accogliente. La sua posizione è perfetta, con una meravigliosa vista mare, splendidi tramonti e accesso diretto alla spiaggia.
				</p>
				<p>
					Si trova a 400 metri dal centro del villaggio ed è situato in una posizione centrale, ideale per l'esplorazione e la visita di vari luoghi di interesse turistico e storico  della Laconia.
				</p>
				<p>
					Gli ospiti possono visitare il castello medievale di Monemvasia, situato a 30 km dall'hotel. Altre escursioni interessanti che possono effettuarsi in una giornata comprendono le Grotte di Diros, la città fortificata di Mystras e la famosa Simos di Elafonisos.
				</p>
			</div>

			<div class="home-gallery">
				<ul>
					<li class="img-1"><img src="../img/img1.jpg" alt="img1" /></li>
					<li class="img-2"><img src="../img/img2.jpg" alt="img1" /></li>
					<li class="img-3"><img src="../img/img3.jpg" alt="img1" /></li>
				</ul>
			</div>

		</div>

		<div class="sidebar grid-1">
			<div class="weather">
				<iframe scrolling="no" frameborder="0" allowtransparency="true" src="http://www.weather.gr/gr/gr/widgets/weather_w2.aspx?p=18036" style="width: 120px; height: 410px"></iframe><a target="blank" style="color: #999999; text-align: center; display: block; font: 10px/10px Arial,san-serif; text-decoration: none;" href="http://www.weather.gr">καιρός weather.gr</a>
			</div>
			<a href="elea_monemvasia_laconia.php#formanchor" class="social-button">Come arrivare</a>
		</div>

	</section>
	
	<!-- INCLUDE FOOTER -->

	<?php include_once('footer_it.php');?>
		
	</body>
</html>
