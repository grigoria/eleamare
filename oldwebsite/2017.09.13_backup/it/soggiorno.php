<?php
	$title = "Elea Mare Hotel:: soggiorno";
	$discription = "Foto e dettagli sul soggiorno all' Elea Mare Hotel.";
	include('header_it.php');
?>

	<body id="accommodation">

	<!-- Google Analytics -->
	
	<?php include_once("../analyticstracking.php") ?>

	<!-- MOBILE MENU - VIEPORT < 640PX -->

	<?php include('navigation_mobile_it.php');?>

  <header id="header">
		
		<!-- MAIN BACKGROUND -->

		<div id="slider" class="nivoSlider">
	    <img src="../img/main-backgrounds/accommodation.jpg" alt="1" />
  	</div>

  	<!-- INCLUDE NAVIGATION & BOOK NOW -->
		
		<?php include_once('navigation_it.php');?>

	</header>

		<section id="main" class="container">
			
			<div class="mainbar grid-4">
				<div class="main-text">
					<h1>Soggiorno - Book direct for Exclusive Offers</h1>
					<p>
						L'albergo dispone di 22 monolocali, tutti spaziosi con aria condizionata, connessione a internet Wi-Fi, angolo cottura, frigorifero, TV, asciugacapelli, telefono e bagno con doccia.
					</p>
					<p class="checkin">Arrivo: 14:00 – 21:00 / Partenza: 7:00 – 12:00</p>
				</div>
			</div>

			<div class="sidebar grid-2">
				<h1>Servizi</h1>
				<ul class="services">
					<li>Wi-Fi gratuito</li>
					<li>Colazione continentale (8:00-12:00)</li>
					<li>Parcheggio</li>
					<li>Noleggio auto</li>
				</ul>
			</div>

			<div class="categories grid-full">
				<h1>Tipologie di Camere</h1>
				<p class="grid-full">* Clicca sulle foto per vedere di più</p>
				
				<div class="type-categories grid-2">
					<a href="../img/accommodation/room1a.jpg" data-lightbox="room1-group" title="Monolocale doppio"><img src="../img/accommodation/room1a.jpg" alt="room1"></a>

					<!-- hidden images for lightbox group -->

					<a href="../img/accommodation/room1b.jpg" class="hidden" data-lightbox="room1-group" title="Monolocale doppio"><img src="../img/accommodation/room1b.jpg" alt="room1"></a>
					<a href="../img/accommodation/room1c.jpg" class="hidden" data-lightbox="room1-group" title="Monolocale doppio"><img src="../img/accommodation/room1c.jpg" alt="room1"></a>
					<h3>Monolocale doppio</h3>
					<ul>
						<li>2 monolocali per disabili</li>
						<li>35 m²</li>
						<li>Tipo di letto: Letti singoli (2)</li>
					</ul>
					<h4>Sottocategorie:</h4>
					<ul>
						<li>- Fronte Mare
							<a href="../img/accommodation/room-1a.jpg" data-lightbox="room-1-a" title="Monolocale doppio - Fronte Mare"><img src="../img/mini-pic.png" alt="mini-pic"></a>
							<a href="../img/accommodation/room-1b.jpg" class="hidden" data-lightbox="room-1-a" title="Monolocale doppio - Fronte Mare"><img src="../img/mini-pic.png" alt="mini-pic"></a></li>
						<li>- Vista Mare Parziale <a href="../img/accommodation/room-1c.jpg" data-lightbox="room-1-b" title="Monolocale doppio - Vista Mare Parziale"><img src="../img/mini-pic.png" alt="mini-pic"></a>
							<a href="../img/accommodation/room-1d.jpg" class="hidden" data-lightbox="room-1-b" title="Monolocale doppio - Vista Mare Parziale"><img src="../img/mini-pic.png" alt="mini-pic"></a></li>
					</ul>
				</div>

				<div class="type-categories grid-2">
					<a href="../img/accommodation/room2a.jpg" data-lightbox="room2-group" title="Monolocale triplo"><img src="../img/accommodation/room2a.jpg" alt="room1"></a>

					<!-- hidden images for lightbox group -->
					
					<a href="../img/accommodation/room2b.jpg" class="hidden" data-lightbox="room2-group" title="Monolocale triplo"><img src="../img/accommodation/room2b.jpg" alt="room1"></a>
					<a href="../img/accommodation/room2c.jpg" class="hidden" data-lightbox="room2-group" title="Monolocale triplo"><img src="../img/accommodation/room2c.jpg" alt="room1"></a>
					<h3>Monolocale triplo</h3>
					<ul>
						<li>Fronte Mare</li>
						<li>35 m²</li>
						<li>Tipo di letto: Letti singoli (3)</li>
					</ul>
				</div>

				<div class="type-categories grid-2">
					<a href="../img/accommodation/room3a.jpg" data-lightbox="room3-group" title="Appartamento con due camere da letto"><img src="../img/accommodation/room3a.jpg" alt="room1"></a>

					<!-- hidden images for lightbox group -->
					
					<a href="../img/accommodation/room3b.jpg" class="hidden" data-lightbox="room3-group" title="Appartamento con due camere da letto"><img src="../img/accommodation/room3b.jpg" alt="room1"></a>
					<a href="../img/accommodation/room3c.jpg" class="hidden" data-lightbox="room3-group" title="Appartamento con due camere da letto"><img src="../img/accommodation/room3c.jpg" alt="room1"></a>
					<h3>Appartamento con due camere da letto</h3>
					<ul>
						<li>Fronte Mare</li>
						<li>50 m²</li>
						<li>Tipo di letto: Letti singoli (4)</li>
					</ul>
				</div>

			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer_it.php');?>

	</body>
</html>
