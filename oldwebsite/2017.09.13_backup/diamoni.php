<?php
	$title = "Ξενοδοχείο Elea Mare:: διαμονή";
	$discription = "Φωτογραφίες και λεπτομέρειες σχετικά με την διαμονή στο Elea Mare Hotel.";
	include('header.php');
?>

	<body id="accommodation">

	<!-- Google Analytics -->
	
	<?php include_once("analyticstracking.php") ?>

	<!-- MOBILE MENU - VIEPORT < 640PX -->

	<?php include('navigation_mobile.php');?>

  <header id="header">
		
		<!-- MAIN BACKGROUND -->

		<div id="slider" class="nivoSlider">
	    <img src="img/main-backgrounds/accommodation.jpg" alt="Διαμονή στο Elea Mare" />
  	</div>

  	<!-- INCLUDE NAVIGATION & BOOK NOW -->
		
		<?php include_once('navigation.php');?>

	</header>

		<section id="main" class="container">
			
			<div class="mainbar grid-4">
				<div class="main-text">
					<h1>Διαμονή - Book direct for Exclusive Offers</h1>
					<p>
						Το ξενοδοχείο διαθέτει 22 studios. Κάθε ένα από αυτά είναι ευρύχωρο με κλιματισμό, Wi-Fi, ατομική κουζίνα, ψυγείο, τηλεόραση, στεγνωτήρα μαλλιών, τηλέφωνο και μπάνιο με ντους. 
					</p>
					<p class="checkin">Check in: 14:00 – 21:00 / Check out: 7:00 – 12:00.</p>
				</div>
			</div>

			<div class="sidebar grid-2">
				<h1>Παροχές</h1>
				<ul class="services">
					<li>Δωρεάν Wi-Fi</li>
					<li>Πρωινό Continental (8:00 - 12:00)</li>
					<li>Xώρος στάθμευσης</li>
					<li>Ενοικίαση αυτοκινήτου</li>
				</ul>
			</div>

			<div class="categories grid-full">
				<h1>Τύποι Δωματίων</h1>
				<p class="grid-full"> * Κάντε click στις φωτογραφίες για να δείτε περισσότερες</p>
				
				<div class="type-categories grid-2">
					<a href="img/accommodation/room1a.jpg" data-lightbox="room1-group" title="Δίκλινο studio"><img src="img/accommodation/room1a.jpg" alt="Ενοικιαζόμενα Δωμάτια"></a>

					<!-- hidden images for lightbox group -->

					<a href="img/accommodation/room1b.jpg" class="hidden" data-lightbox="room1-group" title="Δίκλινο studio"><img src="img/accommodation/room1b.jpg" alt="Δωμάτιο για δύο - Διακοπές στη θάλασσα"></a>
					<a href="img/accommodation/room1c.jpg" class="hidden" data-lightbox="room1-group" title="Δίκλινο studio"><img src="img/accommodation/room1c.jpg" alt="Προορισμός στη θάλασσα"></a>
					<h3>Δίκλινο studio</h3>
					<ul>
						<li>2 studios για άτομα με ειδικές ανάγκες</li>
						<li>35 τετραγωνικά μέτρα</li>
						<li>Τύπος κρεβατιών: Μονά (2)</li>
					</ul>
					<h4>Υποκατηγορίες:</h4>
					<ul>
						<li>- Δίπλα στη θάλασσα 
							<a href="img/accommodation/room-1a.jpg" data-lightbox="room-1-a" title="Δίκλινο studio - Δίπλα στην Θάλασσα"><img src="img/mini-pic.png" alt="Μπαλκόνι στη θάλασσα"></a>
							<a href="img/accommodation/room-1b.jpg" class="hidden" data-lightbox="room-1-a" title="Δίκλινο studio - Δίπλα στην Θάλασσα"><img src="img/mini-pic.png" alt="Διαμέρισμα με μπαλκόνι"></a></li>
						<li>- Mερική θέα στη θάλασσα <a href="img/accommodation/room-1c.jpg" data-lightbox="room-1-b" title="Δίκλινο studio - Mερική θέα στη θάλασσα "><img src="img/mini-pic.png" alt="Φθηνές διακοπές στη θάλασσα"></a>
							<a href="img/accommodation/room-1d.jpg" class="hidden" data-lightbox="room-1-b" title="Δίκλινο studio - Mερική θέα στη θάλασσα "><img src="img/mini-pic.png" alt="Οικονομικές διακοπές με παιδί"></a></li>
					</ul>
				</div>

				<div class="type-categories grid-2">
					<a href="img/accommodation/room2a.jpg" data-lightbox="room2-group" title="Τρίκλινο studio"><img src="img/accommodation/room2a.jpg" alt="Ενοικιαζόμενα Δωμάτια για 3"></a>

					<!-- hidden images for lightbox group -->
					
					<a href="img/accommodation/room2b.jpg" class="hidden" data-lightbox="room2-group" title="Τρίκλινο studio"><img src="img/accommodation/room2b.jpg" alt="Διακοπές με παιδί"></a>
					<a href="img/accommodation/room2c.jpg" class="hidden" data-lightbox="room2-group" title="Τρίκλινο studio"><img src="img/accommodation/room2c.jpg" alt="Θέα στη θάλασσα"></a>
					<h3>Τρίκλινο studio</h3>
					<ul>
						<li>Δίπλα στη θάλασσα</li>
						<li>35 τετραγωνικά μέτρα</li>
						<li>Τύπος κρεβατιών: Μονά (3)</li>
					</ul>
				</div>

				<div class="type-categories grid-2">
					<a href="img/accommodation/room3a.jpg" data-lightbox="room3-group" title="Διαμέρισμα Δύο Δωματίων"><img src="img/accommodation/room3a.jpg" alt="Διαμέρισμα για οικογένειες με παιδιά"></a>

					<!-- hidden images for lightbox group -->
					
					<a href="img/accommodation/room3b.jpg" class="hidden" data-lightbox="room3-group" title="Διαμέρισμα Δύο Δωματίων"><img src="img/accommodation/room3b.jpg" alt="Διακοπές για οικογένειες με παιδιά"></a>
					<a href="img/accommodation/room3c.jpg" class="hidden" data-lightbox="room3-group" title="Διαμέρισμα Δύο Δωματίων"><img src="img/accommodation/room3c.jpg" alt="Οικογενειακές διακοπές"></a>
					<h3>Διαμέρισμα Δύο Δωματίων</h3>
					<ul>
						<li>Δίπλα στη θάλασσα</li>
						<li>50 τετραγωνικά μέτρα</li>
						<li>Τύπος κρεβατιών: Μονά (4)</li>
					</ul>
				</div>

			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer.php');?>

	</body>
</html>
