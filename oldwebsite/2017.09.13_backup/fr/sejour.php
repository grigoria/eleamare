﻿<?php
	$title = "Hôtel Elea Mare:: séjour";
	$discription = "Photos et détails concernant votre séjour à Elea Mare Hôtel.";
	include('header_fr.php');
?>

	<body id="accommodation">

	<!-- Google Analytics -->
	
	<?php include_once("../analyticstracking.php") ?>

	<!-- MOBILE MENU - VIEPORT < 640PX -->

	<?php include('navigation_mobile_fr.php');?>

  <header id="header">
		
		<!-- MAIN BACKGROUND -->

		<div id="slider" class="nivoSlider">
	    <img src="../img/main-backgrounds/accommodation.jpg" alt="1" />
  	</div>

  	<!-- INCLUDE NAVIGATION & BOOK NOW -->
		
		<?php include_once('navigation_fr.php');?>

	</header>

		<section id="main" class="container">
			
			<div class="mainbar grid-4">
				<div class="main-text">
					<h1>Séjour - Book direct for Exclusive Offers</h1>
					<p>
						L' hôtel dispose de 22 studios. Ils sont spacieux avec air conditionné, connexion Wi-Fi, kitchenette, réfrigérateur, TV, sèche-cheveux, téléphone et salle de bains avec douche.
					</p>
					<p class="checkin">Arrivée: 14:00-21:00 / Départ: 7:00-12:00</p>
				</div>
			</div>

			<div class="sidebar grid-2">
				<h1>Facilités</h1>
				<ul class="services">
					<li>Wi-Fi gratuit</li>
					<li>Petit déjeuner continental (8:00-12:00)</li>
					<li>Parking</li>
					<li>Location de voiture</li>
				</ul>
			</div>

			<div class="categories grid-full">
				<h1>Types de chambres</h1>
				<p class="grid-full">* Cliquez sur les photos pour voir plus</p>
				
				<div class="type-categories grid-2">
					<a href="../img/accommodation/room1a.jpg" data-lightbox="room1-group" title="Double studio"><img src="../img/accommodation/room1a.jpg" alt="room1"></a>

					<!-- hidden images for lightbox group -->

					<a href="../img/accommodation/room1b.jpg" class="hidden" data-lightbox="room1-group" title="Double studio"><img src="../img/accommodation/room1b.jpg" alt="room1"></a>
					<a href="../img/accommodation/room1c.jpg" class="hidden" data-lightbox="room1-group" title="Double studio"><img src="../img/accommodation/room1c.jpg" alt="room1"></a>
					<h3>Double studio</h3>
					<ul>
						<li>2 studios pour personnes handicapées</li>
						<li>35 m²</li>
						<li>Type de lit:  Lits simples (2)</li>
					</ul>
					<h4>Sous – categories:</h4>
					<ul>
						<li>- Bord de Mer
							<a href="../img/accommodation/room-1a.jpg" data-lightbox="room-1-a" title="Double studio - Bord de Mer"><img src="../img/mini-pic.png" alt="mini-pic"></a>
							<a href="../img/accommodation/room-1b.jpg" class="hidden" data-lightbox="room-1-a" title="Double studio - Bord de Mer"><img src="../img/mini-pic.png" alt="mini-pic"></a></li>
						<li>- Vue partielle sur la Mer <a href="../img/accommodation/room-1c.jpg" data-lightbox="room-1-b" title="Double studio - Vue partielle sur la Mer"><img src="../img/mini-pic.png" alt="mini-pic"></a>
							<a href="../img/accommodation/room-1d.jpg" class="hidden" data-lightbox="room-1-b" title="Double studio - Vue partielle sur la Mer"><img src="../img/mini-pic.png" alt="mini-pic"></a></li>
					</ul>
				</div>

				<div class="type-categories grid-2">
					<a href="../img/accommodation/room2a.jpg" data-lightbox="room2-group" title="Triple studio"><img src="../img/accommodation/room2a.jpg" alt="room1"></a>

					<!-- hidden images for lightbox group -->
					
					<a href="../img/accommodation/room2b.jpg" class="hidden" data-lightbox="room2-group" title="Triple studio"><img src="../img/accommodation/room2b.jpg" alt="room1"></a>
					<a href="../img/accommodation/room2c.jpg" class="hidden" data-lightbox="room2-group" title="Triple studio"><img src="../img/accommodation/room2c.jpg" alt="room1"></a>
					<h3>Studio Triple</h3>
					<ul>
						<li>Bord de Mer</li>
						<li>35 m²</li>
						<li>Type de lits: Lits simples (3)</li>
					</ul>
				</div>

				<div class="type-categories grid-2">
					<a href="../img/accommodation/room3a.jpg" data-lightbox="room3-group" title="Two-bedroom apartment"><img src="../img/accommodation/room3a.jpg" alt="room1"></a>

					<!-- hidden images for lightbox group -->
					
					<a href="../img/accommodation/room3b.jpg" class="hidden" data-lightbox="room3-group" title="Appartement à deux chambres"><img src="../img/accommodation/room3b.jpg" alt="room1"></a>
					<a href="../img/accommodation/room3c.jpg" class="hidden" data-lightbox="room3-group" title="Appartement à deux chambres"><img src="../img/accommodation/room3c.jpg" alt="room1"></a>
					<h3>Appartement à deux chambres</h3>
					<ul>
						<li>Bord de Mer</li>
						<li>50 m²</li>
						<li>Type de lits: Lits simples (4)</li>
					</ul>
				</div>

			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer_fr.php');?>

	</body>
</html>
