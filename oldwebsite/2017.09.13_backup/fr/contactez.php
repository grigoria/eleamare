<?php
	$title = "Hôtel Elea Mare:: contactez";
	$discription = "Contactez-nous. Hôtel Elea Mare.";
	include('header_fr.php');
?>

	<body id="contact">

		<!-- Google Analytics -->
	
		<?php include_once("../analyticstracking.php") ?>

		<!-- MOBILE MENU - VIEPORT < 640PX -->

		<?php include('navigation_mobile_fr.php');?>

		<header id="header">

			<!-- MAIN BACKGROUND -->

			<div id="slider" class="nivoSlider">
        <img src="../img/main-backgrounds/contact.jpg" alt="1" />
      </div>

      <!-- INCLUDE NAVIGATION & BOOK NOW -->
		
			<?php include_once('navigation_fr.php');?>

		</header>

		<section id="main" class="container">

			<div class="mainbar grid-4">
				<div class="main-text">
					<a name="form1anchor" id="form1anchor"></a>
					<h1>Formulaire de Contact - Book direct for Exclusive Offers</h1>
					<div class="contact-form grid-full">
            <?php
              if ($_GET['msg'] !="ok") {
            ?>
	            <form class="contact_form" method="post" action="mail-contact_fr.php">
	                <div class="clear">
	                    <label for="name"> Nom / Prénom * :</label>
	                    <input type="text" id="name" name="name" placeholder="Nom / Prénom " required/>
	                </div>
	                <div class="clear">
	                    <label for="country">  Ville / Pays :</label>
	                    <input type="text" id="country" name="country" placeholder="Ville / Pays" />
	                </div>
	                <div class="clear">
	                    <label for="phone"> Téléphone :</label>
	                    <input type="text" id="phone" name="phone"  placeholder="Téléphone"  onkeypress="return isNumber(event)" />
	                </div>
	                <div class="clear">
	                    <label for="email"> E-mail * :</label>
	                    <input type="email" id="email" name="email"  placeholder="E-mail" required/>
	                </div>
	                <div class="clear">
	                    <label for="message">  Message * :</label>
	                    <textarea id="message" name="message"  placeholder="Message" required></textarea>
	                </div>
	                <div class="clear">
	                    <button type="submit" class="button2" value="Envoyer">
	                    <em>Envoyer</em>
	                    </button>
	                    <input id="submitted" type="hidden" name="submitted" value="true" />
	                </div>
	            </form>
	            <?php
            		} else {
                	echo "<div id='sentmsg'>Votre message a été envoyé avec succès. Merci beaucoup.</div>";
            		}
              ?>
	        </div>
				</div>
			</div>

			<div class="sidebar grid-2">
				<h1>Détails de Contact</h1>
				<p>Hôtel Elea Mare </p>
				<ul class="services">
					<li>Elea Monemvasia, Grèce</li>
					<li>Code postal: 23052</li>
					<li>Téléphone: +30 27320 57540-1</li>
					<li>Fax: +30 27320 57551</li>
					<li>E-mail: eleamare@gmail.com</li>
				</ul>
				<p>Réception - Centre des appels: 7:00 -21:00</p>
				<a href="disponibilite.php#form2anchor" class="social-button">VÉRIFIER LA DISPONIBILITÉ</a>
			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer_fr.php');?>
		
	</body>
</html>
