<?php
	$title = "Elea Mare Hotel:: accommodation";
	$discription = "Photos and details regarding the stay at Elea Mare Hotel.";
	include('header_en.php');
?>

	<body id="accommodation">

	<!-- Google Analytics -->
	
	<?php include_once("../analyticstracking.php") ?>

	<!-- MOBILE MENU - VIEPORT < 640PX -->

	<?php include('navigation_mobile_en.php');?>

  <header id="header">
		
		<!-- MAIN BACKGROUND -->

		<div id="slider" class="nivoSlider">
	    <img src="../img/main-backgrounds/accommodation.jpg" alt="Accomodation at Elea Mare Hotel" />
  	</div>

  	<!-- INCLUDE NAVIGATION & BOOK NOW -->
		
		<?php include_once('navigation_en.php');?>

	</header>

		<section id="main" class="container">
			
			<div class="mainbar grid-4">
				<div class="main-text">
					<h1>Accommodation - Book direct for Exclusive Offers</h1>
					<p>
						The hotel offers 22 studios. All of them are spacious with air-condition, Wi-Fi, kitchenette, fridge, TV, hairdryer, telephone and bathroom with shower.
					</p>
					<p class="checkin">Check in: 14:00 – 21:00 / Check out: 7:00 – 12:00.</p>
				</div>
			</div>

			<div class="sidebar grid-2">
				<h1>Facilities</h1>
				<ul class="services">
					<li>Complimentary Wi-Fi</li>
					<li>Continental Breakfast (8:00 – 12:00)</li>
					<li>Parking</li>
					<li>Car rental</li>
				</ul>
			</div>

			<div class="categories grid-full">
				<h1>Rooms Types</h1>
				<p class="grid-full"> * Click on the photos to see more</p>
				
				<div class="type-categories grid-2">
					<a href="../img/accommodation/room1a.jpg" data-lightbox="room1-group" title="Double studio"><img src="../img/accommodation/room1a.jpg" alt="Rooms to let"></a>

					<!-- hidden images for lightbox group -->

					<a href="../img/accommodation/room1b.jpg" class="hidden" data-lightbox="room1-group" title="Double studio"><img src="../img/accommodation/room1b.jpg" alt="Bed and breakfast"></a>
					<a href="../img/accommodation/room1c.jpg" class="hidden" data-lightbox="room1-group" title="Double studio"><img src="../img/accommodation/room1c.jpg" alt="Rooms with balcony at the sea"></a>
					<h3>Double studio</h3>
					<ul>
						<li>2 studios for people with special needs</li>
						<li>35 m²</li>
						<li>Bed Type: Single Beds (2)</li>
					</ul>
					<h4>Subcategories:</h4>
					<ul>
						<li>- Seafront
							<a href="../img/accommodation/room-1a.jpg" data-lightbox="room-1-a" title="Double studio - Seafront"><img src="../img/mini-pic.png" alt="Cheap holidays by the sea"></a>
							<a href="../img/accommodation/room-1b.jpg" class="hidden" data-lightbox="room-1-a" title="Double studio - Seafront"><img src="../img/mini-pic.png" alt="Economical holidays by the sea"></a></li>
						<li>- Partial Sea View <a href="../img/accommodation/room-1c.jpg" data-lightbox="room-1-b" title="Double studio - Partial Sea View"><img src="../img/mini-pic.png" alt="Economical holiday destinations in Greece"></a>
							<a href="../img/accommodation/room-1d.jpg" class="hidden" data-lightbox="room-1-b" title="Double studio - Partial Sea View"><img src="../img/mini-pic.png" alt="Studios by the sea"></a></li>
					</ul>
				</div>

				<div class="type-categories grid-2">
					<a href="../img/accommodation/room2a.jpg" data-lightbox="room2-group" title="Triple studio"><img src="../img/accommodation/room2a.jpg" alt="Holiday destinations with child"></a>

					<!-- hidden images for lightbox group -->
					
					<a href="../img/accommodation/room2b.jpg" class="hidden" data-lightbox="room2-group" title="Triple studio"><img src="../img/accommodation/room2b.jpg" alt="Family destination"></a>
					<a href="../img/accommodation/room2c.jpg" class="hidden" data-lightbox="room2-group" title="Triple studio"><img src="../img/accommodation/room2c.jpg" alt="Room for family in Peloponnese, Lakonia"></a>
					<h3>Triple studio</h3>
					<ul>
						<li>Seafront</li>
						<li>35 m²</li>
						<li>Bed Type: Single Beds (3)</li>
					</ul>
				</div>

				<div class="type-categories grid-2">
					<a href="../img/accommodation/room3a.jpg" data-lightbox="room3-group" title="Two-bedroom apartment"><img src="../img/accommodation/room3a.jpg" alt="Family room in Elea Mare Hotel"></a>

					<!-- hidden images for lightbox group -->
					
					<a href="../img/accommodation/room3b.jpg" class="hidden" data-lightbox="room3-group" title="Two-bedroom apartment"><img src="../img/accommodation/room3b.jpg" alt="Holiday destination for family by the sea in Elea Monemvasia of Lakonia"></a>
					<a href="../img/accommodation/room3c.jpg" class="hidden" data-lightbox="room3-group" title="Two-bedroom apartment"><img src="../img/accommodation/room3c.jpg" alt="Balcony at the sea"></a>
					<h3>Two-bedroom apartment</h3>
					<ul>
						<li>Seafront</li>
						<li>50 m²</li>
						<li>Bed Type: Single Beds (4)</li>
					</ul>
				</div>

			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer_en.php');?>

	</body>
</html>
