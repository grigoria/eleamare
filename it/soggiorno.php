<?php

	$title = "Elea Mare Hotel:: soggiorno";

	$discription = "Foto e dettagli sul soggiorno all' Elea Mare Hotel.";

	include('header_it.php');

?>



	<body id="accommodation">



	<!-- Google Analytics -->



	<?php include_once("../analyticstracking.php") ?>



	<!-- MOBILE MENU - VIEPORT < 640PX -->



	<?php include('navigation_mobile_it.php');?>



  <header id="header">



		<!-- MAIN BACKGROUND -->



		<div id="slider" class="nivoSlider">

	    <img src="../img/main-backgrounds/accommodation.jpg" alt="1" />

  	</div>



  	<!-- INCLUDE NAVIGATION & BOOK NOW -->



		<?php include_once('navigation_it.php');?>



	</header>



		<section id="main" class="container">



			<div class="mainbar grid-4">

				<div class="main-text">

					<h1>Soggiorno - Book direct for Exclusive Offers</h1>

					<p>

						L'albergo dispone di 22 monolocali, tutti spaziosi con aria condizionata, connessione a internet Wi-Fi, angolo cottura, frigorifero, TV, asciugacapelli, telefono e bagno con doccia.<br><span style="font-style:italic">* Animali non ammessi</span>.

					</p>

					<p class="checkin">Arrivo: 14:00 – 21:00 / Partenza: 7:00 – 12:00</p>

				</div>

			</div>



			<div class="sidebar grid-2">

				<h1>Servizi</h1>

				<ul class="services">

					<li>Wi-Fi gratuito</li>

					<li>Colazione continentale (8:00-12:00)</li>

					<li>Parcheggio</li>

					<li>Noleggio auto</li>

				</ul>

			</div>



			<div class="categories grid-full">

				<h1>Tipologie di Camere</h1>

				<p class="grid-full"> * Clicca sulle foto per vedere di più</p>

				<h2>Doppio</h2>

				<div class="type-categories grid-2">

					<a href="../img/accommodation/new/double-studio-partial-sea-view-1.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View"><img src="../img/accommodation/new/double-studio-partial-sea-view-1.jpg" alt="Δίdouble-studio-partial-sea-view"></a>
					<a href="../img/accommodation/new/double-studio-partial-sea-view-2.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View" class="hidden"></a>
					<a href="../img/accommodation/new/double-studio-partial-sea-view-3.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View" class="hidden"></a>
					<a href="../img/accommodation/new/double-studio-partial-sea-view-4.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View" class="hidden"></a>

					<h3>Monolocale Doppio Vista Mare Parziale
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 2 Letti Singoli<br>
								- 35 m²<br>
								- Al piano terra
							</span>
						</span>
					</h3>

				</div>



					<!-- hidden images for lightbox group -->

					<div class="type-categories grid-2">
						<a href="../img/accommodation/new/double-seafront-1.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront"><img src="../img/accommodation/new/double-seafront-1.jpg" alt="double-seafront"></a>
						<a href="../img/accommodation/new/double-seafront-2.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront" class="hidden"></a>
						<a href="../img/accommodation/new/double-seafront-3.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront" class="hidden"></a>
						<a href="../img/accommodation/new/double-seafront-4.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront" class="hidden"></a>
					<h3>Monolocale Doppio Standard Fronte Mare
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 2 Letti Singoli<br>
								- 35 m²<br>
								- Al piano terra o al piano superiore secondo la disponibilità
							</span>
						</span>
					</h3>

				</div>

				<div class="type-categories grid-2">
					<a href="../img/accommodation/new/superior-double-1.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront"><img src="../img/accommodation/new/superior-double-1.jpg" alt="superior-double"></a>
					<a href="../img/accommodation/new/superior-double-2.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront" class="hidden"></a>
					<a href="../img/accommodation/new/superior-double-3.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront" class="hidden"></a>
					<a href="../img/accommodation/new/superior-double-4.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront" class="hidden"></a>
				<h3>Monolocale Doppio Superiore Fronte Mare
					<br>
					<span class="cat-info">
						<span class="cat-info-box">
							- 1 Letto King Size di Coco-Mat (2mΧ2m)<br>
							- 35 m²<br>
							- Al piano superiore<br>
							- Cassaforte per computer portatile
						</span>
					</span>
				</h3>

			</div>

		</div>

		<div class="categories grid-full">

			<h2>Triplo</h2>

			<div class="type-categories grid-2">
				<a href="../img/accommodation/new/triple-studio-1.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafront"><img src="../img/accommodation/new/triple-studio-1.jpg" alt="triple-studio"></a>
				<a href="../img/accommodation/new/triple-studio-2.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafron" class="hidden"></a>
				<a href="../img/accommodation/new/triple-studio-3.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafron" class="hidden"></a>
				<a href="../img/accommodation/new/triple-studio-4.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafron" class="hidden"></a>
			<h3>Monolocale Triplo Standard Fronte Mare
				<br>
				<span class="cat-info">
					<span class="cat-info-box">
						- 3 Letti Singoli<br>
						- 35 m²<br>
						- Al piano terra o al piano superiore secondo la disponibilità
					</span>
				</span>
			</h3>

		</div>

		<div class="type-categories grid-2">
			<a href="../img/accommodation/new/superior-triple-1.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront"><img src="../img/accommodation/new/superior-triple-1.jpg" alt="superior-triple"></a>
			<a href="../img/accommodation/new/superior-triple-2.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront" class="hidden"></a>
			<a href="../img/accommodation/new/superior-triple-3.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront" class="hidden"></a>
			<a href="../img/accommodation/new/superior-triple-4.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront" class="hidden"></a>
		<h3>Monolocale Triplo Superiore Fronte Mare
			<br>
			<span class="cat-info">
				<span class="cat-info-box">
					- 1 Letto King Size di Coco-Mat (2mΧ2m) + 1 Letto Singolο<br>
					- 40 m²<br>
					- Al piano superiore<br>
					- Cassaforte per computer portatile
				</span>
			</span>
		</h3>

	</div>

</div>

<div class="categories grid-full">

	<h2>Quadruplo</h2>


	<div class="type-categories grid-2">
		<a href="../img/accommodation/new/standard-two-bedroom-apartment-1.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront"><img src="../img/accommodation/new/standard-two-bedroom-apartment-1.jpg" alt="standard-two-bedroom-apartment"></a>
		<a href="../img/accommodation/new/standard-two-bedroom-apartment-2.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront" class="hidden"></a>
		<a href="../img/accommodation/new/standard-two-bedroom-apartment-3.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront" class="hidden"></a>
		<a href="../img/accommodation/new/standard-two-bedroom-apartment-4.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront" class="hidden"></a>
	<h3>Appartamento Standard con 2 Camere da Letto Fronte Mare
		<br>
		<span class="cat-info">
			<span class="cat-info-box">
				- 4 Letti Singoli<br>
				- 50 m²<br>
				- Al piano terra
			</span>
		</span>
	</h3>

</div>

<div class="type-categories grid-2">
	<a href="../img/accommodation/new/superior-two-bedroom-apartment-1.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"><img src="../img/accommodation/new/superior-two-bedroom-apartment-1.jpg" alt="superior-two-bedroom-apartment"></a>
	<a href="../img/accommodation/new/superior-two-bedroom-apartment-2.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"></a>
	<a href="../img/accommodation/new/superior-two-bedroom-apartment-3.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"></a>
	<a href="../img/accommodation/new/superior-two-bedroom-apartment-4.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"></a>
<h3>
Appartamento Superiore con 2 Camere da Letto Fronte Mare
	<br>
	<span class="cat-info">
		<span class="cat-info-box">
			- 1 Letto King Size di Coco-Mat (2mΧ2m) + 2 Letti Singoli<br>
			- 50 m²<br>
			- Al piano superiore<br>
			- Cassaforte per computer portatile
		</span>
	</span>
</h3>

</div>

</div>

<div class="categories grid-full">

	<h2>Suite Acqua</h2>

	<div class="type-categories grid-2">
		<a href="../img/accommodation/new/acqua-1.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)"><img src="../img/accommodation/new/acqua-1.jpg" alt="acqua"></a>
		<a href="../img/accommodation/new/acqua-2.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
		<a href="../img/accommodation/new/acqua-3.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
		<a href="../img/accommodation/new/acqua-4.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
		<a href="../img/accommodation/new/acqua-5.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
		<a href="../img/accommodation/new/acqua-6.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
		<a href="../img/accommodation/new/acqua-7.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
		<a href="../img/accommodation/new/acqua-8.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
	<h3>
		Suite Acqua (2 Doppie Monolocali Collegati)

		<br>
		<span class="cat-info">
			<span class="cat-info-box">
				- 1 Letto King Size di Coco-Mat (2mΧ2m) + 2 Letti Singoli<br>
				- 60 m² <br>
				- Al piano terra<br>
				- 1 Cassaforte per computer portatile<br>
				- 2 TV<br>
				- 2 Asciugacapelli<br>
				- 2 Bagni<br>
				- 1 Specchio a figura intera
			</span>
		</span>
	</h3>

	</div>



		</section>



		<!-- INCLUDE FOOTER -->



		<?php include_once('footer_it.php');?>



	</body>

</html>
