<?php
  $title = "Elea Mare Hotel:: galleria";
  $discription = "Il territorio circostante di Elea Monemvasia Laconia e distanze dall' Elea Mare Hotel.";
  include('header_it.php');
?>

  <body id="art_gallery">

    <!-- Google Analytics -->

    <?php include_once("../analyticstracking.php") ?>

    <!-- MOBILE MENU - VIEPORT < 640PX -->

    <?php include('navigation_mobile_it.php');?>

    <header id="header">

      <!-- MAIN BACKGROUND -->

      <div id="slider" class="nivoSlider">
        <img src="../img/main-backgrounds/art.jpg" alt="1" />
      </div>

      <!-- INCLUDE NAVIGATION & BOOK NOW -->

      <?php include_once('navigation_it.php');?>

    </header>

    <section id="main" class="container">

      <div class="mainbar grid-full">
        <div class="main-text ">

          <h1>Galleria d'arte</h1>
          <p>
            Nella nostra Galleria il visitatore avrà l'opportunità di vedere creazioni artistici
            locali e non solo, per entrare in contatto con la cultura della zona durante il suo soggiorno.
          </p>
        </div>
      </div>

      <div class="gallery">

        <a href="../img/art/art1.jpg" data-lightbox="image-1" class="grid-2">
          <img src="../img/art/art1.jpg" alt="Elea mare hotel - Galleria d'arte">
        </a>

        <a href="../img/art/art2.jpg" data-lightbox="image-1" class="grid-2">
          <img src="../img/art/art2.jpg" alt="Elea mare hotel - Galleria d'arte">
        </a>


        <a href="../img/art/art3.jpg" data-lightbox="image-1" class="grid-2">
          <img src="../img/art/art3.jpg" alt="Elea mare hotel - Galleria d'arte">
        </a>
      </div>

      <div class="mainbar grid-full art">
        <div class="main-text">

          <h1>Graffiti</h1>
        </div>
      </div>

      <div class="videos">

        <div class="grid-3">
          <iframe src="https://player.vimeo.com/video/172207960" style= "max-width: 640px" width= "100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
          <p><a href="https://vimeo.com/172207960">"Fingerprint" - by LosOtros Mj Tom</a></p>
        </div>

        <div class="grid-3">
          <iframe src="https://player.vimeo.com/video/172203165" style= "max-width: 640px" width= "100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
          <p><a href="https://vimeo.com/172203165">"Eyes" - by Allegra Betti</a></p>
        </div>
    </div>

    </section>

    <!-- INCLUDE FOOTER -->

    <?php include_once('footer_it.php');?>

  </body>
</html>
