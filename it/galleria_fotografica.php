<?php
	$title = "Elea Mare Hotel:: galleria fotografica";
	$discription = "Foto di Elea Mare Hotel";
	include('header_it.php');
?>

	<body id="gallery">

		<!-- Google Analytics -->
	
		<?php include_once("../analyticstracking.php") ?>

		<!-- MOBILE MENU - VIEPORT < 640PX -->

		<?php include('navigation_mobile_it.php');?>

		<header id="header">

			<!-- MAIN BACKGROUND -->

			<div id="slider" class="nivoSlider">
        <img src="../img/main-backgrounds/gallery.jpg" alt="1" />
      </div>

      <!-- INCLUDE NAVIGATION & BOOK NOW -->
		
			<?php include_once('navigation_it.php');?>

		</header>

		<section id="main" class="container">

			<div class="mainbar grid-full">
				<div class="main-text">
					<h1>Galleria fotografica</h1>
					<p>
						Clicca sulle foto per ingrandirle
					</p>
				</div>
			</div>

			<div class="gallery">
				<a href="../img/gallery/pic-1.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-1.jpg" alt="mini-pic"></a>
				<a href="../img/gallery/pic-2.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-2.jpg" alt="mini-pic"></a>
				<a href="../img/gallery/pic-3.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-3.jpg" alt="mini-pic"></a>

				<div class="clear"></div>

				<a href="../img/gallery/pic-4.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-4.jpg" alt="mini-pic"></a>
				<a href="../img/gallery/pic-5.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-5.jpg" alt="mini-pic"></a>
				<a href="../img/gallery/pic-6.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-6.jpg" alt="mini-pic"></a>

				<div class="clear"></div>

				<a href="../img/gallery/pic-7.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-7.jpg" alt="mini-pic"></a>
				<a href="../img/gallery/pic-8.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-8.jpg" alt="mini-pic"></a>
				<a href="../img/gallery/pic-9.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-9.jpg" alt="mini-pic"></a>

				<div class="clear"></div>

				<a href="../img/gallery/pic-10.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-10.jpg" alt="mini-pic"></a>
				<a href="../img/gallery/pic-11.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-11.jpg" alt="mini-pic"></a>
				<a href="../img/gallery/pic-12.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-12-mini.jpg" alt="mini-pic"></a>
			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer_it.php');?>
		
	</body>
</html>
