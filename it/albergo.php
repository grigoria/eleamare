<?php
	$title = "Elea Mare Hotel:: contatto";
	$discription = "Contattateci. Hotel Elea Mare.";
	include('header_it.php');
?>

	<body id="contact">

		<!-- Google Analytics -->
	
		<?php include_once("../analyticstracking.php") ?>

		<!-- MOBILE MENU - VIEPORT < 640PX -->

		<?php include('navigation_mobile_it.php');?>

		<header id="header">

			<!-- MAIN BACKGROUND -->

			<div id="slider" class="nivoSlider">
        <img src="../img/main-backgrounds/contact.jpg" alt="1" />
      </div>

      <!-- INCLUDE NAVIGATION & BOOK NOW -->
		
			<?php include_once('navigation_it.php');?>

		</header>

		<section id="main" class="container">

			<div class="mainbar grid-4">
				<div class="main-text">
					<a name="form1anchor" id="form1anchor"></a>
					<h1>Modulo di Contatto - Book direct for Exclusive Offers</h1>
					<div class="contact-form grid-full">
            <?php
              if ($_GET['msg'] !="ok") {
            ?>
	            <form class="contact_form" method="post" action="mail-contact_it.php">
	                <div class="clear">
	                    <label for="name"> Nome / Cognome * :</label>
	                    <input type="text" id="name" name="name" placeholder="Nome / Cognome " required/>
	                </div>
	                <div class="clear">
	                    <label for="country">  Città / Paese :</label>
	                    <input type="text" id="country" name="country" placeholder="Città / Paese" />
	                </div>
	                <div class="clear">
	                    <label for="phone"> Telefono :</label>
	                    <input type="text" id="phone" name="phone"  placeholder="Telefono"  onkeypress="return isNumber(event)" />
	                </div>
	                <div class="clear">
	                    <label for="email"> E-mail * :</label>
	                    <input type="email" id="email" name="email"  placeholder="E-mail" required/>
	                </div>
	                <div class="clear">
	                    <label for="message">  Messaggio * :</label>
	                    <textarea id="message" name="message"  placeholder="Messaggio" required></textarea>
	                </div>
	                <div class="clear">
	                    <button type="submit" class="button2" value="Inviare">
	                    <em>Inviare</em>
	                    </button>
	                    <input id="submitted" type="hidden" name="submitted" value="true" />
	                </div>
	            </form>
	            <?php
            		} else {
                	echo "<div id='sentmsg'>Il suo messaggio è stato inviato con successo. La ringraziamo per averci contattato.</div>";
            		}
              ?>
	        </div>
				</div>
			</div>

			<div class="sidebar grid-2">
				<h1>Informazioni di Contatto</h1>
				<p>Elea Mare Hotel</p>
				<ul class="services">
					<li>Elea Monemvasia, Grecia</li>
					<li>Codice di Avviamento Postale: 23052</li>
					<li>Telefono: +30 27320 57540-1</li>
					<li>Fax: +30 27320 57551</li>
					<li>E-mail: eleamare@gmail.com</li>
				</ul>
				<p>Ricevimento / Call Center: 7:00-21:00</p>
				<a href="disponibilita.php#form2anchor" class="social-button">VERIFICA DISPONIBILITÀ</a>
			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer_it.php');?>
		
	</body>
</html>
