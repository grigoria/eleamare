<!DOCTYPE html>
<html lang="it" class="no-js">

	<head>
		
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title><?php echo $title; ?></title>
		<meta name="description" content="<?php echo $discription; ?>">
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" href="../stylesheets/screen.css">
		<link rel="stylesheet" href="../stylesheets/nivo-slider.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" />
		<link href="../stylesheets/lightbox.css" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300&subset=greek,latin' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="https://apis.google.com/js/platform.js"></script>
	
	</head>
