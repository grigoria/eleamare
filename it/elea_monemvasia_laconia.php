<?php
	$title = "Elea Mare Hotel:: posizione";
	$discription = "Il territorio circostante di Elea Monemvasia Laconia e distanze dall' Elea Mare Hotel.";
	include('header_it.php');
?>

	<body id="location">

		<!-- Google Analytics -->
	
		<?php include_once("../analyticstracking.php") ?>

		<!-- MOBILE MENU - VIEPORT < 640PX -->

		<?php include('navigation_mobile_it.php');?>

		<header id="header">

			<!-- MAIN BACKGROUND -->
			
			<div id="slider" class="nivoSlider">
        <img src="../img/main-backgrounds/location.jpg" alt="1" />
      </div>
			
			<!-- INCLUDE NAVIGATION & BOOK NOW -->
		
			<?php include_once('navigation_it.php');?>
 
		</header>

		<section id="main" class="container">
			
			<div class="mainbar grid-3">

				<div class="main-text">
					<a name="formanchor" id="formanchor"></a>
					<div class="infos">
						<h1>Come arrivare in auto:</h1>
						<ul>
							<li>• Dall'Aeroporto di Atene: 307 km (3 ore e 40 minuti)</li>
							<li>• Dall'Aeroporto di Kalamata: 153 km (2 ore)</li>
							<li>• Dal Porto di Patrasso: 310 km (4 ore)</li>
							<li>• Dal Porto di Monemvasia: 30 km (30 minuti)</li>
						</ul>    
					</div>
				</div>
			</div>

			<div class="sidebar grid-3">
				<h1>Mappa</h1>
				<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=Elea+Mare+Hotel,+Eparchiaki+Odos+Elaias-Daimonias,+Monemvasia,+%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1&amp;hl=el&amp;ie=UTF8&amp;sll=36.765429,22.83165&amp;sspn=0.08031,0.169086&amp;oq=elea+&amp;hq=Elea+Mare+Hotel,&amp;hnear=%CE%95%CF%80%CE%B1%CF%81%CF%87%CE%B9%CE%B1%CE%BA%CE%AE+%CE%9F%CE%B4%CF%8C%CF%82+%CE%95%CE%BB%CE%B1%CE%AF%CE%B1%CF%82-%CE%94%CE%B1%CE%B9%CE%BC%CE%BF%CE%BD%CE%B9%CE%AC%CF%82,+%CE%9C%CE%BF%CE%BD%CE%B5%CE%BC%CE%B2%CE%B1%CF%83%CE%B9%CE%AC,+%CE%9B%CE%B1%CE%BA%CF%89%CE%BD%CE%AF%CE%B1,+%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1&amp;t=m&amp;ll=36.746587,22.799377&amp;spn=0.321318,0.676346&amp;z=9&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?q=Elea+Mare+Hotel,+Eparchiaki+Odos+Elaias-Daimonias,+Monemvasia,+%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1&amp;hl=el&amp;ie=UTF8&amp;sll=36.765429,22.83165&amp;sspn=0.08031,0.169086&amp;oq=elea+&amp;hq=Elea+Mare+Hotel,&amp;hnear=%CE%95%CF%80%CE%B1%CF%81%CF%87%CE%B9%CE%B1%CE%BA%CE%AE+%CE%9F%CE%B4%CF%8C%CF%82+%CE%95%CE%BB%CE%B1%CE%AF%CE%B1%CF%82-%CE%94%CE%B1%CE%B9%CE%BC%CE%BF%CE%BD%CE%B9%CE%AC%CF%82,+%CE%9C%CE%BF%CE%BD%CE%B5%CE%BC%CE%B2%CE%B1%CF%83%CE%B9%CE%AC,+%CE%9B%CE%B1%CE%BA%CF%89%CE%BD%CE%AF%CE%B1,+%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1&amp;t=m&amp;ll=36.746587,22.799377&amp;spn=0.321318,0.676346&amp;source=embed" style="color:#0000FF;text-align:left">View larger map</a></small>
			</div>

			<div class="distance grid-full">
				<h1>Dall'albergo a</h1>
				<table>
					<tr>
						<td class="highlight">- Atene 300 km</td>
						<td>- Elea 400m</td>
						<td class="highlight">- Kiparissi 50 km</td>
						<td>- Sparta 60 km</td>
					</tr>
					<tr>
						<td>- Molai 8 km</td>
						<td>- Kosmas 55 km</td>
						<td>- Plitra 12 km</td>
						<td class="highlight">- Monemvasia 30 km</td>
					</tr>
					<tr>
						<td class="highlight">- Mistras 65 km</td>
						<td>- Karavostassi 12 km</td>
						<td>- Monastero di Elona 60km</td>
						<td>- Neapoli 50 km</td>
					</tr>
					<tr>
						<td>- Arcangelos 20 km</td>
						<td>- Monastero di Evangelistria 42km</td>
						<td class="highlight">- Elafonisos 48 km</td>
						<td>- Githeio 45 km</td>
					</tr>
					<tr>
						<td class="highlight">- Mani, Grotte Diros 65 km</td>
						<td class="highlight">- Kithira (da Neapoli)</td>
					</tr>
				</table>
			</div>

			<div class="categories beaches grid-full">
				<h1>Spiagge di Elea:</h1>
				<ul>
					<li class="type-categories grid-1-5">
						<a href="../img/location/beach1.jpg" data-lightbox="viandini" title="Vandini"><img src="../img/location/beach1.jpg" alt="beach"></a>
						<h3>Vandini</h3>
						<h4>50m dall'albergo</h4>
					</li>
					<li class="type-categories grid-1-5">
						<a href="../img/location/beach2.jpg" data-lightbox="tigania" title="Tigania"><img src="../img/location/beach2.jpg" alt="beach"></a>
						<h3>Tigania</h3>
						<h4>700m dall'albergo</h4>
					</li>
					<li class="type-categories grid-1-5">
						<a href="../img/location/beach3.jpg" data-lightbox="limani" title"Limani(Porto)"><img src="../img/location/beach3.jpg" alt="beach"></a>
						<h3>Limani (Porto)</h3>
						<h4>400m dall'albergo</h4>
					</li>
					<li class="type-categories grid-1-5">
						<a href="../img/location/beach4.jpg" data-lightbox="kavos" title"Kavos"><img src="../img/location/beach4.jpg" alt="beach"></a>
						<h3>Kavos</h3>
						<h4>1.400m dall'albergo</h4>
					</li>
				</ul>
			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer_it.php');?>

	</body>
</html>
