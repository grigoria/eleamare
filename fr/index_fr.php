﻿	<?php
	$title = "Hôtel Elea Mare à Elea de Monemvasia, Lakonie";
	$discription = "Á Elea, un pittoresque village de pêcheurs qui appartient à la municipalité de Monemvasia, est situé l' hôtel Elea Mare dans un environnement calme et convivial. L' emplacement est parfait avec une vue impecable sur la mer, magnifiques couchers de soleil et un accès direct à la plage. Il est à 400 mètres du centre de la village et se trouve dans une position centrale, qu' est idéal pour la navigation à toutes les belles et historiques domaines de la Laconie.";
	include('header_fr.php');
?>

	<body id="homepage">

	<!-- Google Analytics -->
	
	<?php include_once("../analyticstracking.php") ?>

	<!-- PRELOADER -->

	<div class="modal">
		<div class="pre-logo"></div>
	</div>

	<!-- MOBILE MENU - VIEPORT < 640PX -->

	<?php include('navigation_mobile_fr.php');?>

	<header id="header">
		
		<!-- SLIDER -->

		<div id="slider" class="nivoSlider">
	    <img src="../img/slider/slider.jpg" alt="1" />
	    <img src="../img/slider/slider2.jpg" alt="2" />
	    <img src="../img/slider/slider3.jpg" alt="3" />
	    <img src="../img/slider/slider4.jpg" alt="4" />
	    <img src="../img/slider/slider5.jpg" alt="5" />
	    <img src="../img/slider/slider6.jpg" alt="6" />
  	</div>

  <!-- INCLUDE NAVIGATION & BOOK NOW -->
		
	<?php include_once('navigation_fr.php');?>

	</header>
		
	<section id="main" class="container">
		
		<div class="mainbar grid-5">
			
			<div class="main-text">
				<h1>Bienvenue à Elea Mare,</h1>
				<h2>au coeur de Laconie…</h2>
				<p>
					L' hôtel Elea Mare caractérisé par son ambiance calme et conviviale se situe dans le pittoresque village de pêcheurs, Elea, qui appartient à la municipalité de Monemvasia. L'atout majeur est son emplacement avec la superbe vue mer, les magnifiques couchers de soleil et l'accès direct à la plage.
				</p>
				<p>
					Il se trouve à 400 mètres du centre du village et sa position centrale est le point de repère pour découvrir tous les beaux paysages et les sites historiques dans la région de Laconie.
				</p>
				<p>
					Les visiteurs peuvent explorer le château médiéval de Monemvasia, situé à 30 km de l' hôtel. En plus, l'hôtel offre une base de départ idéale pour vos excursions d'une journée aux Grottes de Diros, à la ville fortifiée de Mystras et à la fameuse plage de Simos, au dos de l'îlot d'Elafonissos.
				</p>
			</div>

			<div class="home-gallery">
				<ul>
					<li class="img-1"><img src="../img/img1.jpg" alt="img1" /></li>
					<li class="img-2"><img src="../img/img2.jpg" alt="img1" /></li>
					<li class="img-3"><img src="../img/img3.jpg" alt="img1" /></li>
				</ul>
			</div>

		</div>

		<div class="sidebar grid-1">
			<div class="weather">
				<iframe scrolling="no" frameborder="0" allowtransparency="true" src="http://www.weather.gr/gr/gr/widgets/weather_w2.aspx?p=18036" style="width: 120px; height: 410px"></iframe><a target="blank" style="color: #999999; text-align: center; display: block; font: 10px/10px Arial,san-serif; text-decoration: none;" href="http://www.weather.gr">καιρός weather.gr</a>
			</div>
			<a href="elia_monemvasia_laconia.php#formanchor" class="social-button">Comment accés</a>
		</div>

	</section>
	
	<!-- INCLUDE FOOTER -->

	<?php include_once('footer_fr.php');?>
		
	</body>
</html>
