<?php
  $title = "Hôtel Elea Mare:: galerie";
  $discription = "La région environnante d' Elea Monemvasia Lakonie, la carte et les distances d' Elea Mare Hôtel.";
  include('header_fr.php');
?>

  <body id="art_gallery">

    <!-- Google Analytics -->

    <?php include_once("../analyticstracking.php") ?>

    <!-- MOBILE MENU - VIEPORT < 640PX -->

    <?php include('navigation_mobile_fr.php');?>

    <header id="header">

      <!-- MAIN BACKGROUND -->

      <div id="slider" class="nivoSlider">
        <img src="../img/main-backgrounds/art.jpg" alt="1" />
      </div>

      <!-- INCLUDE NAVIGATION & BOOK NOW -->

      <?php include_once('navigation_fr.php');?>

    </header>

    <section id="main" class="container">

      <div class="mainbar grid-full">
        <div class="main-text ">

          <h1>Galerie d'art</h1>
          <p>
            Dans notre Galerie, le visiteur aura l' occasion de voir des œuvres artistiques locales
            et pas seulement, pour entrer en contact avec la culture de notre région.
          </p>
        </div>
      </div>

      <div class="gallery">

        <a href="../img/art/art1.jpg" data-lightbox="image-1" class="grid-2">
          <img src="../img/art/art1.jpg" alt="Elea mare hotel - Galerie d'art">
        </a>

        <a href="../img/art/art2.jpg" data-lightbox="image-1" class="grid-2">
          <img src="../img/art/art2.jpg" alt="Elea mare hotel - Galerie d'art">
        </a>


        <a href="../img/art/art3.jpg" data-lightbox="image-1" class="grid-2">
          <img src="../img/art/art3.jpg" alt="Elea mare hotel - Galerie d'art">
        </a>
      </div>

      <div class="mainbar grid-full art">
        <div class="main-text">

          <h1>Graffiti</h1>
        </div>
      </div>

      <div class="videos">

        <div class="grid-3">
          <iframe src="https://player.vimeo.com/video/172207960" style= "max-width: 640px" width= "100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
          <p><a href="https://vimeo.com/172207960">"Fingerprint" - by LosOtros Mj Tom</a></p>
        </div>

        <div class="grid-3">
          <iframe src="https://player.vimeo.com/video/172203165" style= "max-width: 640px" width= "100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
          <p><a href="https://vimeo.com/172203165">"Eyes" - by Allegra Betti</a></p>
        </div>
    </div>

    </section>

    <!-- INCLUDE FOOTER -->

    <?php include_once('footer_fr.php');?>

  </body>
</html>
