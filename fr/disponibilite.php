﻿<?php
	$title = "Hôtel Elea Mare:: vérifier la disponibilité";
	$discription = "Vérifier la disponibilité. Hôtel Elea Mare.";
	include('header_fr.php');
?>

	<body id="contact-form">

		<!-- Google Analytics -->
	
		<?php include_once("../analyticstracking.php") ?>

		<!-- MOBILE MENU - VIEPORT < 640PX -->

		<?php include('navigation_mobile_fr.php');?>

		<header id="header">

			<!-- MAIN BACKGROUND -->

			<div id="slider" class="nivoSlider">
        <img src="../img/main-backgrounds/contact.jpg" alt="1" />
      </div>
			
			<!-- INCLUDE NAVIGATION & BOOK NOW -->
		
			<?php include_once('navigation_fr.php');?>
     
		</header>

		<section id="main" class="container">
			
			<div class="mainbar grid-4">
				<div class="main-text">
					<a name="form2anchor" id="form2anchor"></a>
					<h1>Vérifier la disponibilité - Book direct for Exclusive Offers</h1>
					<div class="contact-form grid-full">
						<?php
              if ($_GET['msg'] !="ok") {
            ?>

	            <form class="contact_form" method="post" action="check_fr.php">
	                <div class="clear">
	                    <label for="checkinnew"> Arrivée * :</label>
	                    <input type="text" id="checkinnew" name="checkinnew" value="<?php echo htmlspecialchars($_POST['checkin']) ?>" required/>
	                </div>
	                <div class="clear">
	                    <label for="checkoutnew">  Départ * :</label>
	                    <input type="text" id="checkoutnew" name="checkoutnew" value="<?php echo htmlspecialchars($_POST['checkout']) ?>" required/>
	                </div>
	                <div class="clear">
	                    <label for="roomsnew"> Chambres * :</label>
	                    <input type="text" id="roomsnew" name="roomsnew" value="<?php echo $_POST['rooms']; ?>" onkeypress="return isNumber(event)" required/>
	                </div>
	                <div class="clear">
	                    <label for="peoplenew"> Personnes * :</label>
	                    <input type="text" id="peoplenew" name="peoplenew" value="<?php echo $_POST['adults']; ?>" onkeypress="return isNumber(event)" required/>
	                </div>
	                <div class="clear">
	                	<label for="bedsnew" style="padding-top: 18px;"> Type de lit supplémentaire: :</label>
		                <select name="bedsnew" id="bedsnew">
		       							<option value="0">Aucun</option>
		       							<option value="1">Lit supplémentaire (3-16 ans)</option>
		        						<option value="2">Lit bébé (1-2 ans)</option>
		        					</select>
		        				</div>
	                <div class="clear">
	                    <label for="name"> Nom / Prénom * :</label>
	                    <input type="text" id="name" name="name" placeholder="Nom / Prénom " required/>
	                </div>
	                <div class="clear">
	                    <label for="country">  Ville / Pays:</label>
	                    <input type="text" id="country" name="country" placeholder="Ville / Pays" />
	                </div>
	                <div class="clear">
	                    <label for="phone"> Téléphone * :</label>
	                    <input type="text" id="phone" name="phone" placeholder="Téléphone"  onkeypress="return isNumber(event)" required/>
	                </div>
	                 <div class="clear">
	                    <label for="email"> E-mail * :</label>
	                    <input type="email" id="email" name="email" placeholder="E-mail" required/>
	                </div>
	                <div class="clear">
	                    <label for="message">  Message * :</label>
	                    <textarea id="message" name="message" placeholder="Message" required></textarea>
	                </div>
	                <div class="clear">
	                    <button type="submit" class="button2" value="Envoyer">
	                    <em>Envoyer</em>
	                    </button>
	                    <input id="submitted" type="hidden" name="submitted" value="true" />
	                </div>
	            </form>

	            <?php
            		} else {
                	echo "<div id='sentmsg'>Votre message a été envoyé avec succès. Receverez notre offre immédiatement au e-mail que vous avez indiqué. Merci beaucoup.</div>";
            		}
              ?>
	        </div>
				</div>
			</div>

			<div class="sidebar grid-2">
				<h1>Détails de Contact</h1>
				<p>Hôtel Elea Mare </p>
				<ul class="services">
					<li>Elea Monemvasia, Grèce</li>
					<li>Code postal: 23052</li>
					<li>Téléphone: +30 27320 57540-1</li>
					<li>Fax: +30 27320 57551</li>
					<li>E-mail: eleamare@gmail.com</li>
				</ul>
				<p>Réception - Centre des appels: 7:00 -21:00</p>
			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer_fr.php');?>
		
	</body>
</html>
