<nav id="nav2" class="nav">
  <ul>
		<li><a href="index_fr.php">Accueil</a></li>
    <li><a href="sejour.php">Séjour</a></li>
    <li><a href="elia_monemvasia_laconia.php">Région</a></li>
    <li><a href="galerie_de_photos.php">Galerie de photos</a></li>
    <li><a href="galerie.php">Galerie d'art'</a></li>
    <li><a href="contactez.php">Contactez</a></li>
	</ul>
</nav>

<script>
  //  The function to change the class
  var changeClass = function (r,className1,className2) {
  var regex = new RegExp("(?:^|\\s+)" + className1 + "(?:\\s+|$)");
  if( regex.test(r.className) ) {
  r.className = r.className.replace(regex,' '+className2+' ');
  }
  else{
    r.className = r.className.replace(new RegExp("(?:^|\\s+)" + className2 + "(?:\\s+|$)"),' '+className1+' ');
  }
    return r.className;
  };

  //  Creating our button in JS for smaller screens
  var menuElements = document.getElementById('nav2');
  menuElements.insertAdjacentHTML('afterBegin','<button type="button" id="menutoggle" class="navtoogle" aria-hidden="true"><i aria-hidden="true" class="icon-menu"> </i></button>');

  //  Toggle the class on click to show / hide the menu
  document.getElementById('menutoggle').onclick = function() {
  changeClass(this, 'navtoogle active', 'navtoogle');
  }
</script>
