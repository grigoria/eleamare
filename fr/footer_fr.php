<div class="upper-footer container">

	<div class="boxe separator grid-full"></div>

	<ul class="social grid-3">
		<li class="facebook">
			<a href="https://www.facebook.com/eleamarehotel" target="_blank">
				facebook
			</a>
		</li>
		<li class="twitter">
			<a href="https://twitter.com/eleamare" target="_blank">
				twitter
			</a>
		</li>
		<li class="google">
			<a href="https://plus.google.com/+EleamareGr/posts" target="_blank">
				google
			</a>
		</li>
		<li class="pinterest">
			<a href="http://www.pinterest.com/eleamare" target="_blank">
				pinterest
			</a>
		</li>
		<li class="instagram">
			<a href="http://instagram.com/eleamare" target="_blank">
				instagram
			</a>
		</li>
	</ul>

	<ul class="external grid-3">
		<li class="tripadvisor">
			<a href="http://www.tripadvisor.com/Hotel_Review-g5427703-d1575752-Reviews-Elea_Mare_Hotel-Elea_Laconia_Region_Peloponnese.html" target="_blank">
				tripadvisor
			</a>
		</li>
		<li class="eot">
			<a href="http://visitgreece.gr/" target="_blank">
				eot
			</a>
		</li>
	</ul>

	<div class="g-plusone" data-annotation="none" data-href="http://www.eleamare.gr"></div>

	<div class="facebook-like"><iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Feleamarehotel&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe></div>

</div>

<footer id="footer" class="container">

	<div class="foot-left grid-4">
		<ul>
			<li><a href="index_fr.php">Accueil</a></li>
	    <li><a href="sejour.php">Séjour</a></li>
	    <li><a href="elia_monemvasia_laconia.php">Région</a></li>
	    <li><a href="galerie_de_photos.php">Galerie de photos</a></li>
      <li><a href="galerie.php">Galerie d'art</a></li>
	    <li><a href="contactez.php">Contactez</a></li>
		</ul>
		<p>
			© 2018 Elea Mare Hotel. All rights reserved.
		</p>
	</div>

	<div class="foot-right grid-2">
		Elea Monemvasia, Greece<br/>
		tel: +30 27320 57540 | eleamare@gmail.com
	</div>

</footer>

<a href="#" class="scrollup">Scroll</a>

<script type="text/javascript" src="../js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="../js/jquery.nivo.slider.js"></script>

<script type="text/javascript">
  $(window).load(function() {
  $('#slider').nivoSlider({
    effect:'fade', // Specify sets like: 'fold,fade,sliceDown'
    animSpeed:500, // Slide transition speed
    pauseTime:7000, // How long each slide will show
    startSlide:0, // Set starting Slide (0 index)
    directionNav:false, // Next & Prev navigation
    directionNavHide:false, // Only show on hover
    controlNav:false, // 1,2,3... navigation
    controlNavThumbs:false, // Use thumbnails for Control Nav
    controlNavThumbsFromRel:false, // Use image rel for thumbs
    controlNavThumbsSearch: '.jpg', // Replace this with...
    controlNavThumbsReplace: '_thumb.jpg', // ...this in thumb Image src
    keyboardNav:true, // Use left & right arrows
    pauseOnHover:false, // Stop animation while hovering
    manualAdvance:false, // Force manual transitions
    captionOpacity:0.8, // Universal caption opacity
    prevText: 'Prev', // Prev directionNav text
    nextText: 'Next' // Next directionNav text
    });
	});
</script>

<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="../js/lightbox-2.6.min.js"></script>
<script>
  $(function() {
    $( "#datepicker" ).datepicker();
    $( "#datepicker2" ).datepicker();
  });
</script>

<script type="text/javascript">
	$(document).ready(function(){

		$(window).scroll(function(){
		  if ($(this).scrollTop() > 100) {
		    $('.scrollup').fadeIn();
		  } else {
		    $('.scrollup').fadeOut();
		  }
		});

		$('.scrollup').click(function(){
		  $("html, body").animate({ scrollTop: 0 }, 600);
		    return false;
		});
	});
</script>

<!-- preloader script -->

<script>
	jQuery(window).load(function(){
		$(".modal").delay(1000).fadeOut(1000);
	});
</script>

<script>
	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
</script>
