﻿<?php

	$title = "Hôtel Elea Mare:: séjour";

	$discription = "Photos et détails concernant votre séjour à Elea Mare Hôtel.";

	include('header_fr.php');

?>



	<body id="accommodation">



	<!-- Google Analytics -->



	<?php include_once("../analyticstracking.php") ?>



	<!-- MOBILE MENU - VIEPORT < 640PX -->



	<?php include('navigation_mobile_fr.php');?>



  <header id="header">



		<!-- MAIN BACKGROUND -->



		<div id="slider" class="nivoSlider">

	    <img src="../img/main-backgrounds/accommodation.jpg" alt="1" />

  	</div>



  	<!-- INCLUDE NAVIGATION & BOOK NOW -->



		<?php include_once('navigation_fr.php');?>



	</header>



		<section id="main" class="container">



			<div class="mainbar grid-4">

				<div class="main-text">

					<h1>Séjour - Book direct for Exclusive Offers</h1>

					<p>

						L' hôtel dispose de 22 studios. Ils sont spacieux avec air conditionné, connexion Wi-Fi, kitchenette, réfrigérateur, TV, sèche-cheveux, téléphone et salle de bains avec douche.<br><span style="font-style:italic">  * Les animaux de compagnie ne sont pas admis</span>

					</p>

					<p class="checkin">Arrivée: 14:00-21:00 / Départ: 7:00-12:00</p>

				</div>

			</div>



			<div class="sidebar grid-2">

				<h1>Facilités</h1>

				<ul class="services">

					<li>Wi-Fi gratuit</li>

					<li>Petit déjeuner continental (8:00-12:00)</li>

					<li>Parking</li>

					<li>Location de voiture</li>

				</ul>

			</div>



			<div class="categories grid-full">

				<h1>Types de chambres</h1>

				<p class="grid-full"> * Cliquez sur les photos pour voir plus</p>

				<h2>Double</h2>

				<div class="type-categories grid-2">

					<a href="../img/accommodation/new/double-studio-partial-sea-view-1.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View"><img src="../img/accommodation/new/double-studio-partial-sea-view-1.jpg" alt="Δίdouble-studio-partial-sea-view"></a>
					<a href="../img/accommodation/new/double-studio-partial-sea-view-2.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View" class="hidden"></a>
					<a href="../img/accommodation/new/double-studio-partial-sea-view-3.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View" class="hidden"></a>
					<a href="../img/accommodation/new/double-studio-partial-sea-view-4.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View" class="hidden"></a>

					<h3>Studio Double Vue Partielle sur la Mer
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 2 Lits Simples<br>
								- 35 m²<br>
								- Au rez de chaussée
							</span>
						</span>
					</h3>

				</div>



					<!-- hidden images for lightbox group -->

					<div class="type-categories grid-2">
						<a href="../img/accommodation/new/double-seafront-1.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront"><img src="../img/accommodation/new/double-seafront-1.jpg" alt="double-seafront"></a>
						<a href="../img/accommodation/new/double-seafront-2.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront" class="hidden"></a>
						<a href="../img/accommodation/new/double-seafront-3.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront" class="hidden"></a>
						<a href="../img/accommodation/new/double-seafront-4.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront" class="hidden"></a>
					<h3>Studio Double Standard Bord de Mer
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 2 Lits Simples<br>
								- 35 m²<br>
								- Au rez de chaussée ou à l'étage selon disponibilité
							</span>
						</span>
					</h3>

				</div>

				<div class="type-categories grid-2">
					<a href="../img/accommodation/new/superior-double-1.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront"><img src="../img/accommodation/new/superior-double-1.jpg" alt="superior-double"></a>
					<a href="../img/accommodation/new/superior-double-2.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront" class="hidden"></a>
					<a href="../img/accommodation/new/superior-double-3.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront" class="hidden"></a>
					<a href="../img/accommodation/new/superior-double-4.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront" class="hidden"></a>
				<h3>Studio Double Supérieur Bord de Mer
					<br>
					<span class="cat-info">
						<span class="cat-info-box">
							- 1 Lit King Size de Coco-Mat (2mΧ2m)<br>
							- 35 m²<br>
							- Ά l'étage supérieur<br>
							- Coffre-fort pour ordinateur portable
						</span>
					</span>
				</h3>

			</div>

		</div>

		<div class="categories grid-full">

			<h2>Triple</h2>

			<div class="type-categories grid-2">
				<a href="../img/accommodation/new/triple-studio-1.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafront"><img src="../img/accommodation/new/triple-studio-1.jpg" alt="triple-studio"></a>
				<a href="../img/accommodation/new/triple-studio-2.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafront" class="hidden"></a>
				<a href="../img/accommodation/new/triple-studio-3.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafront" class="hidden"></a>
				<a href="../img/accommodation/new/triple-studio-4.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafront" class="hidden"></a>
			<h3>Studio Triple Standard Bord de Mer
				<br>
				<span class="cat-info">
					<span class="cat-info-box">
						- 3 Lits Simples<br>
						- 35 m²<br>
						- Au rez de chaussée ou à l'étage selon disponibilité
					</span>
				</span>
			</h3>

		</div>

		<div class="type-categories grid-2">
			<a href="../img/accommodation/new/superior-triple-1.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront"><img src="../img/accommodation/new/superior-triple-1.jpg" alt="superior-triple"></a>
			<a href="../img/accommodation/new/superior-triple-2.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront" class="hidden"></a>
			<a href="../img/accommodation/new/superior-triple-3.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront" class="hidden"></a>
			<a href="../img/accommodation/new/superior-triple-4.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront" class="hidden"></a>
		<h3>Studio Triple Supérieur Bord de Mer
			<br>
			<span class="cat-info">
				<span class="cat-info-box">
					- 1 Lit King Size de Coco-Mat (2mΧ2m) + 1 Lit Simple<br>
					- 40 m²<br>
					- Ά l'étage supérieur<br>
					- Coffre-fort pour ordinateur portable
				</span>
			</span>
		</h3>

	</div>

</div>

<div class="categories grid-full">

	<h2>Quadruple</h2>


	<div class="type-categories grid-2">
		<a href="../img/accommodation/new/standard-two-bedroom-apartment-1.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront"><img src="../img/accommodation/new/standard-two-bedroom-apartment-1.jpg" alt="standard-two-bedroom-apartment"></a>
		<a href="../img/accommodation/new/standard-two-bedroom-apartment-2.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront" class="hidden"></a>
		<a href="../img/accommodation/new/standard-two-bedroom-apartment-3.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront" class="hidden"></a>
		<a href="../img/accommodation/new/standard-two-bedroom-apartment-4.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront" class="hidden"></a>
	<h3>Appartement Standard 2 Chambres Bord de Mer
		<br>
		<span class="cat-info">
			<span class="cat-info-box">
				- 4 Lits Simples<br>
				- 50 m²<br>
				- Au rez de chaussée
			</span>
		</span>
	</h3>

</div>

<div class="type-categories grid-2">
	<a href="../img/accommodation/new/superior-two-bedroom-apartment-1.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"><img src="../img/accommodation/new/superior-two-bedroom-apartment-1.jpg" alt="superior-two-bedroom-apartment"></a>
	<a href="../img/accommodation/new/superior-two-bedroom-apartment-2.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"></a>
	<a href="../img/accommodation/new/superior-two-bedroom-apartment-3.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"></a>
	<a href="../img/accommodation/new/superior-two-bedroom-apartment-4.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"></a>
<h3>
Appartement Supérieur 2 Chambres Bord de Mer
	<br>
	<span class="cat-info">
		<span class="cat-info-box">
			- 1 Lit King Size de Coco-Mat (2mΧ2m) + 2 Lits Simples<br>
			- 50 m²<br>
			- Ά l'étage supérieur<br>
			- Coffre-fort pour ordinateur portable
		</span>
	</span>
</h3>

</div>

</div>

<div class="categories grid-full">

	<h2>Suite Acqua</h2>

	<div class="type-categories grid-2">
		<a href="../img/accommodation/new/acqua-1.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios) "><img src="../img/accommodation/new/acqua-1.jpg" alt="acqua"></a>
		<a href="../img/accommodation/new/acqua-2.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios) " class="hidden"></a>
		<a href="../img/accommodation/new/acqua-3.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios) " class="hidden"></a>
		<a href="../img/accommodation/new/acqua-4.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios) " class="hidden"></a>
		<a href="../img/accommodation/new/acqua-5.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios) " class="hidden"></a>
		<a href="../img/accommodation/new/acqua-6.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios) " class="hidden"></a>
		<a href="../img/accommodation/new/acqua-7.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios) " class="hidden"></a>
		<a href="../img/accommodation/new/acqua-8.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios) " class="hidden"></a>
	<h3>
		Suite Acqua (2 Doubles Studios Communicants)

		<br>
		<span class="cat-info">
			<span class="cat-info-box">
				- 1 Lit King Size de Coco-Mat (2mΧ2m) + 2 Lits Simples<br>
				- 60 m²<br>
				- Au rez de chaussée <br>
				- 1 Coffre-fort pour ordinateur portable<br>
				- 2 TVs<br>
				- 2 Sèche-cheveux<br>
				- 2 Salles de bains<br>
				- 1 Μiroir pleine longueur
			</span>
		</span>
	</h3>

	</div>



		</section>



		<!-- INCLUDE FOOTER -->



		<?php include_once('footer_fr.php');?>



	</body>

</html>
