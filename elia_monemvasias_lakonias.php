<?php
	$title = "Ξενοδοχείο Elea Mare:: περιοχή";
	$discription = "Η ευρύτερη περιοχή της Ελιάς Μονεμβασίας Λακωνίας, χάρτης και αποστάσεις από το Elea Mare Hotel.";
	include('header.php');
?>

	<body id="location">

		<!-- Google Analytics -->
	
		<?php include_once("analyticstracking.php") ?>

		<!-- MOBILE MENU - VIEPORT < 640PX -->

		<?php include('navigation_mobile.php');?>

		<header id="header">

			<!-- MAIN BACKGROUND -->
			
			<div id="slider" class="nivoSlider">
        <img src="img/main-backgrounds/location.jpg" alt="1" />
      </div>
			
			<!-- INCLUDE NAVIGATION & BOOK NOW -->
		
			<?php include_once('navigation.php');?>
 
		</header>

		<section id="main" class="container">
			
			<div class="mainbar grid-3">

				<div class="main-text">
					<a name="formanchor" id="formanchor"></a>
					<div class="infos">
						<h1>Πώς θα έρθετε οδικώς:</h1>
						<ul>
							<li>•	Από Αεροδρόμιο Ελ. Βενιζέλος: 307 χλμ (3 ώρες και 40 λεπτά)</lI>
							<li>•	Από Αεροδρόμιο Καλαμάτας: 153 χλμ (2 ώρες)</li>
							<li>•	Από Λιμάνι Πάτρας: 310 χλμ (4 ώρες)</li>
							<li>•	Από Λιμάνι Μονεμβασιάς: 30 χλμ (30 λεπτά)</li>
						</ul>    
					</div>
				</div>
			</div>

			<div class="sidebar grid-3">
				<h1>Χάρτης</h1>
				<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=Elea+Mare+Hotel,+Eparchiaki+Odos+Elaias-Daimonias,+Monemvasia,+%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1&amp;hl=el&amp;ie=UTF8&amp;sll=36.765429,22.83165&amp;sspn=0.08031,0.169086&amp;oq=elea+&amp;hq=Elea+Mare+Hotel,&amp;hnear=%CE%95%CF%80%CE%B1%CF%81%CF%87%CE%B9%CE%B1%CE%BA%CE%AE+%CE%9F%CE%B4%CF%8C%CF%82+%CE%95%CE%BB%CE%B1%CE%AF%CE%B1%CF%82-%CE%94%CE%B1%CE%B9%CE%BC%CE%BF%CE%BD%CE%B9%CE%AC%CF%82,+%CE%9C%CE%BF%CE%BD%CE%B5%CE%BC%CE%B2%CE%B1%CF%83%CE%B9%CE%AC,+%CE%9B%CE%B1%CE%BA%CF%89%CE%BD%CE%AF%CE%B1,+%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1&amp;t=m&amp;ll=36.746587,22.799377&amp;spn=0.321318,0.676346&amp;z=9&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?q=Elea+Mare+Hotel,+Eparchiaki+Odos+Elaias-Daimonias,+Monemvasia,+%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1&amp;hl=el&amp;ie=UTF8&amp;sll=36.765429,22.83165&amp;sspn=0.08031,0.169086&amp;oq=elea+&amp;hq=Elea+Mare+Hotel,&amp;hnear=%CE%95%CF%80%CE%B1%CF%81%CF%87%CE%B9%CE%B1%CE%BA%CE%AE+%CE%9F%CE%B4%CF%8C%CF%82+%CE%95%CE%BB%CE%B1%CE%AF%CE%B1%CF%82-%CE%94%CE%B1%CE%B9%CE%BC%CE%BF%CE%BD%CE%B9%CE%AC%CF%82,+%CE%9C%CE%BF%CE%BD%CE%B5%CE%BC%CE%B2%CE%B1%CF%83%CE%B9%CE%AC,+%CE%9B%CE%B1%CE%BA%CF%89%CE%BD%CE%AF%CE%B1,+%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1&amp;t=m&amp;ll=36.746587,22.799377&amp;spn=0.321318,0.676346&amp;source=embed" style="color:#0000FF;text-align:left">View larger map</a></small>
			</div>

			<div class="distance grid-full">
				<h1>Από το Ξενοδοχείο προς</h1>
				<table>
					<tr>
						<td class="highlight">- Αθήνα 300 χλμ</td>
						<td>- Ελαία 400 μέτρα</td>
						<td class="highlight">- Κυπαρίσσι 50 χλμ</td>
						<td>- Σπάρτη 60 χλμ</td>
					</tr>
					<tr>
						<td>- Μολάοι 8 χλμ</td>
						<td>- Κοσμάς 55 χλμ</td>
						<td>- Πλύτρα 12 χλμ</td>
						<td class="highlight">- Μονεμβάσια 30 χλμ</td>
					</tr>
					<tr>
						<td class="highlight">- Μυστράς 65 χλμ</td>
						<td>- Καραβοστάσι 12 χλμ</td>
						<td>- Μονή Έλωνας 60 χλμ</td>
						<td>- Νεάπολη 50 χλμ</td>
					</tr>
					<tr>
						<td>- Αρχάγγελος 20 χλμ</td>
						<td>- Μονή Ευαγγελίστριας 42 χλμ</td>
						<td class="highlight">- Ελαφόνησος 48 χλμ</td>
						<td>- Γύθειο 45 χλμ</td>
					</tr>
					<tr>
						<td class="highlight">- Μάνη, Σπήλαια Δυρού 65 χλμ</td>
						<td class="highlight">- Κύθηρα (από Νεάπολη)</td>
					</tr>
				</table>
			</div>

			<div class="categories beaches grid-full">
				<h1>Παραλίες της Ελιάς</h1>
				<ul>
					<li class="type-categories grid-1-5">
						<a href="img/location/beach1.jpg" data-lightbox="viandini" title="Βιανδίνη"><img src="img/location/beach1.jpg" alt="beach"></a>
						<h3>Βιανδίνη</h3>
						<h4>50μ. από το ξενοδοχείο</h4>
					</li>
					<li class="type-categories grid-1-5">
						<a href="img/location/beach2.jpg" data-lightbox="tigania" title="Τηγάνια"><img src="img/location/beach2.jpg" alt="beach"></a>
						<h3>Τηγάνια</h3>
						<h4>700μ. από το ξενοδοχείο</h4>
					</li>
					<li class="type-categories grid-1-5">
						<a href="img/location/beach3.jpg" data-lightbox="limani" title="Λιμάνι"><img src="img/location/beach3.jpg" alt="beach"></a>
						<h3>Λιμάνι</h3>
						<h4>400μ. από το ξενοδοχείο</h4>
					</li>
					<li class="type-categories grid-1-5">
						<a href="img/location/beach4.jpg" data-lightbox="kavos" title="Κάβος"><img src="img/location/beach4.jpg" alt="beach"></a>
						<h3>Κάβος</h3>
						<h4>1400μ. από το ξενοδοχείο</h4>
					</li>
				</ul>
			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer.php');?>

	</body>
</html>
