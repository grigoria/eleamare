<?php
	$title = "Ξενοδοχείο Elea Mare:: φωτογραφίες";
	$discription = "Φωτογραφίες του ξενοδοχείου Elea Mare";
	include('header.php');
?>

	<body id="gallery">

		<!-- Google Analytics -->
	
		<?php include_once("analyticstracking.php") ?>

		<!-- MOBILE MENU - VIEPORT < 640PX -->

		<?php include('navigation_mobile.php');?>

		<header id="header">

			<!-- MAIN BACKGROUND -->

			<div id="slider" class="nivoSlider">
        <img src="img/main-backgrounds/gallery.jpg" alt="Ηλιοβασίλεμα στην Ελιά Μονεμβασίας" />
      </div>

      <!-- INCLUDE NAVIGATION & BOOK NOW -->
		
			<?php include_once('navigation.php');?>

		</header>

		<section id="main" class="container">

			<div class="mainbar grid-full">
				<div class="main-text">
					<h1>Φωτογραφίες</h1>
					<p>
						Κάντε click στις φωτογραφίες για να τις δείτε σε μεγαλύτερο μέγεθος.
					</p>
				</div>
			</div>

			<div class="gallery">
				<a href="img/gallery/pic-1.jpg" data-lightbox="image-1" class="grid-2"><img src="img/gallery/pic-1.jpg" alt="Προορισμός για ήσυχες διακοπές"></a>
				<a href="img/gallery/pic-2.jpg" data-lightbox="image-1" class="grid-2"><img src="img/gallery/pic-2.jpg" alt="Προορισμός για οικονομικές διακοπές"></a>
				<a href="img/gallery/pic-3.jpg" data-lightbox="image-1" class="grid-2"><img src="img/gallery/pic-3.jpg" alt="Θέα στη θάλασσα"></a>

				<div class="clear"></div>

				<a href="img/gallery/pic-4.jpg" data-lightbox="image-1" class="grid-2"><img src="img/gallery/pic-4.jpg" alt="Θέα στη θάλασσα 2"></a>
				<a href="img/gallery/pic-5.jpg" data-lightbox="image-1" class="grid-2"><img src="img/gallery/pic-5.jpg" alt="Πάρκινγκ ξενοδοχείου"></a>
				<a href="img/gallery/pic-6.jpg" data-lightbox="image-1" class="grid-2"><img src="img/gallery/pic-6.jpg" alt="Ξενοδοχείο στη θάλασσα"></a>

				<div class="clear"></div>

				<a href="img/gallery/pic-7.jpg" data-lightbox="image-1" class="grid-2"><img src="img/gallery/pic-7.jpg" alt="Μπαλκόνι με θέα στη θάλασσα"></a>
				<a href="img/gallery/pic-8.jpg" data-lightbox="image-1" class="grid-2"><img src="img/gallery/pic-8.jpg" alt="Οικονομικός προορισμός"></a>
				<a href="img/gallery/pic-9.jpg" data-lightbox="image-1" class="grid-2"><img src="img/gallery/pic-9.jpg" alt="Φθηνός προορισμός"></a>

				<div class="clear"></div>

				<a href="img/gallery/pic-10.jpg" data-lightbox="image-1" class="grid-2"><img src="img/gallery/pic-10.jpg" alt="Ενοικιαζόμενα δωμάτια στη θάλασσα"></a>
				<a href="img/gallery/pic-11.jpg" data-lightbox="image-1" class="grid-2"><img src="img/gallery/pic-11.jpg" alt="Προορισμός διακοπών στη θάλασσα"></a>
				<a href="img/gallery/pic-12.jpg" data-lightbox="image-1" class="grid-2"><img src="img/gallery/pic-12-mini.jpg" alt="Δωμάτιο με μπαλκόνι στη θάλασσα"></a>
			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer.php');?>
		
	</body>
</html>
