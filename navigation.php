<div class="menu">
  <div class="container">
		<nav class="mobile">
			<h1 class="logo">
				<a href="index.php" class="logo-link">
					Elea Mare
				</a>
			</h1>
			<ul>
				<li><a href="index.php">Αρχική</a></li>
				<li><a href="diamoni.php">Διαμονή</a></li>
				<li><a href="elia_monemvasias_lakonias.php">Περιοχή</a></li>
				<li><a href="fotografies.php">Φωτογραφίες</a></li>
				<li><a href="gallery.php">Γκαλερί</a></li>
				<li><a href="ksenodoxeio.php">Επικοινωνία</a></li>
			</ul>
		</nav>
	</div>
</div>

<div class="book">

	<div class="container">

		<div class="lang">
			<ul>
				<li class="greek"><a href="index.php">greek</a></li>
				<li class="english"><a href="en/index_en.php">english</a></li>
				<li class="france"><a href="fr/index_fr.php">french</a></li>
				<li class="italy"><a href="it/index_it.php">Italian</a></li>
			</ul>
		</div>

		<div class="book-fields">
			<form id="booking_form" action="kratiseis.php#form2anchor"  method="post">
				<ul>
					<li>
						<input type="text" name="checkin" value="Άφιξη" class="book-input date-pick" id="datepicker" required/>
					</li>
				  <li>
						<input type="text" name="checkout" value="Αναχώρηση" class="book-input date-pick" id="datepicker2" required/>
					</li>
					<li>
						<select name="rooms" id="rooms"  class="book-select" required>
	       			<option value="0">Δωμάτια</option>
	        		<option value="1">1</option><option value="2">2</option>
	        		<option value="3">3</option><option value="4">4</option>
	        		<option value="5">5</option>
	        	</select>
					</li>
					<li>
						<select name="adults" id="adults"  class="book-select" required>
	        		<option value="0">Άτομα</option>
							<option value="1">1</option><option value="2">2</option>
							<option value="3">3</option><option value="4">4</option>
						</select>
					</li>
					<li>
						<input type="submit" class="btn" value="ΕΛΕΓΧΟΣ ΔΙΑΘΕΣΙΜΟΤΗΤΑΣ"/>
					</li>
				</ul>
			</form>


		</div>

	</div>

</div>

<div class="logo-mobile"></div>
