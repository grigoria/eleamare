<?php
	$title = "Elea Mare Hotel:: photo gallery";
	$discription = "Photos of Elea Mare Hotel";
	include('header_en.php');
?>

	<body id="gallery">

		<!-- Google Analytics -->
	
		<?php include_once("../analyticstracking.php") ?>

		<!-- MOBILE MENU - VIEPORT < 640PX -->

		<?php include('navigation_mobile_en.php');?>

		<header id="header">

			<!-- MAIN BACKGROUND -->

			<div id="slider" class="nivoSlider">
        <img src="../img/main-backgrounds/gallery.jpg" alt="Sunset from Elea Mare Hotel" />
      </div>

      <!-- INCLUDE NAVIGATION & BOOK NOW -->
		
			<?php include_once('navigation_en.php');?>

		</header>

		<section id="main" class="container">

			<div class="mainbar grid-full">
				<div class="main-text">
					<h1>Photo Gallery</h1>
					<p>
						Click on the photos to enlarge
					</p>
				</div>
			</div>

			<div class="gallery">
				<a href="../img/gallery/pic-1.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-1.jpg" alt="Destination for relaxing holidays"></a>
				<a href="../img/gallery/pic-2.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-2.jpg" alt="Destination for family holidays"></a>
				<a href="../img/gallery/pic-3.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-3.jpg" alt="Family vacation"></a>

				<div class="clear"></div>

				<a href="../img/gallery/pic-4.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-4.jpg" alt="Destination for honeymoon in Greece, Peloponnese"></a>
				<a href="../img/gallery/pic-5.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-5.jpg" alt="Elea Mare Hotel by the sea"></a>
				<a href="../img/gallery/pic-6.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-6.jpg" alt="View at the sea from your room"></a>

				<div class="clear"></div>

				<a href="../img/gallery/pic-7.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-7.jpg" alt="Holidays with children"></a>
				<a href="../img/gallery/pic-8.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-8.jpg" alt="Bed and breakfast"></a>
				<a href="../img/gallery/pic-9.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-9.jpg" alt="Studios by the sea"></a>

				<div class="clear"></div>

				<a href="../img/gallery/pic-10.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-10.jpg" alt="Economical destination for holidays"></a>
				<a href="../img/gallery/pic-11.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-11.jpg" alt="Destination for holidays with children"></a>
				<a href="../img/gallery/pic-12.jpg" data-lightbox="image-1" class="grid-2"><img src="../img/gallery/pic-12-mini.jpg" alt="Explore south of Greece, Peloponnese, Lakonia, Monemvasia"></a>
			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer_en.php');?>
		
	</body>
</html>
