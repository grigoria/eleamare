<?php
	$title = "Elea Mare Hotel:: location";
	$discription = "The surrounding area of Elea Monemvasia Lakonia, map and distances from Elea Mare Hotel.";
	include('header_en.php');
?>

	<body id="location">

		<!-- Google Analytics -->
	
		<?php include_once("../analyticstracking.php") ?>

		<!-- MOBILE MENU - VIEPORT < 640PX -->

		<?php include('navigation_mobile_en.php');?>

		<header id="header">

			<!-- MAIN BACKGROUND -->
			
			<div id="slider" class="nivoSlider">
        <img src="../img/main-backgrounds/location.jpg" alt="Elia Monemvasia, Lakonia, Peloponnese" />
      </div>
			
			<!-- INCLUDE NAVIGATION & BOOK NOW -->
		
			<?php include_once('navigation_en.php');?>
 
		</header>

		<section id="main" class="container">
			
			<div class="mainbar grid-3">

				<div class="main-text">
					<a name="formanchor" id="formanchor"></a>
					<div class="infos">
						<h1>How to reach us by car?</h1>
						<ul>
							<li>• From Athens Airport: 307 km (3 hours and 40 minutes)</li>
							<li>• From Kalamata Airport: 153 km (2 hours)</li>
							<li>• From Patras Port: 310 km (4 hours)</li>
							<li>• From Monemvasia Port: 30 km (30 minutes)</li>
						</ul>    
					</div>
				</div>
			</div>

			<div class="sidebar grid-3">
				<h1>Map</h1>
				<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=Elea+Mare+Hotel,+Eparchiaki+Odos+Elaias-Daimonias,+Monemvasia,+%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1&amp;hl=el&amp;ie=UTF8&amp;sll=36.765429,22.83165&amp;sspn=0.08031,0.169086&amp;oq=elea+&amp;hq=Elea+Mare+Hotel,&amp;hnear=%CE%95%CF%80%CE%B1%CF%81%CF%87%CE%B9%CE%B1%CE%BA%CE%AE+%CE%9F%CE%B4%CF%8C%CF%82+%CE%95%CE%BB%CE%B1%CE%AF%CE%B1%CF%82-%CE%94%CE%B1%CE%B9%CE%BC%CE%BF%CE%BD%CE%B9%CE%AC%CF%82,+%CE%9C%CE%BF%CE%BD%CE%B5%CE%BC%CE%B2%CE%B1%CF%83%CE%B9%CE%AC,+%CE%9B%CE%B1%CE%BA%CF%89%CE%BD%CE%AF%CE%B1,+%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1&amp;t=m&amp;ll=36.746587,22.799377&amp;spn=0.321318,0.676346&amp;z=9&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?q=Elea+Mare+Hotel,+Eparchiaki+Odos+Elaias-Daimonias,+Monemvasia,+%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1&amp;hl=el&amp;ie=UTF8&amp;sll=36.765429,22.83165&amp;sspn=0.08031,0.169086&amp;oq=elea+&amp;hq=Elea+Mare+Hotel,&amp;hnear=%CE%95%CF%80%CE%B1%CF%81%CF%87%CE%B9%CE%B1%CE%BA%CE%AE+%CE%9F%CE%B4%CF%8C%CF%82+%CE%95%CE%BB%CE%B1%CE%AF%CE%B1%CF%82-%CE%94%CE%B1%CE%B9%CE%BC%CE%BF%CE%BD%CE%B9%CE%AC%CF%82,+%CE%9C%CE%BF%CE%BD%CE%B5%CE%BC%CE%B2%CE%B1%CF%83%CE%B9%CE%AC,+%CE%9B%CE%B1%CE%BA%CF%89%CE%BD%CE%AF%CE%B1,+%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1&amp;t=m&amp;ll=36.746587,22.799377&amp;spn=0.321318,0.676346&amp;source=embed" style="color:#0000FF;text-align:left">View larger map</a></small>
			</div>

			<div class="distance grid-full">
				<h1>From the Hotel to:</h1>
				<table>
					<tr>
						<td class="highlight">- Athens 300 km</td>
						<td>- Elea 400 meters</td>
						<td class="highlight">- Kiparissi 50 km</td>
						<td>- Sparta 60 km</td>
					</tr>
					<tr>
						<td>- Molai 8 km</td>
						<td>- Kosmas 55 km</td>
						<td>- Plitra 12 km</td>
						<td class="highlight">- Monemvasia 30 km</td>
					</tr>
					<tr>
						<td class="highlight">- Mistras 65 km</td>
						<td>- Karavostassi 12 km</td>
						<td>- Elona Monastery 60 km</td>
						<td>- Neapoli 50 km</td>
					</tr>
					<tr>
						<td>- Archangelos 20 km</td>
						<td>- Evangelistria Monastery 42 km</td>
						<td class="highlight">- Elafonisos 48 km</td>
						<td>- Githeio 45 km</td>
					</tr>
					<tr>
						<td class="highlight">- Mani, Diros Caves 65 km</td>
						<td class="highlight">- Kithira (from Neapoli)</td>
					</tr>
				</table>
			</div>

			<div class="categories beaches grid-full">
				<h1>Beaches of Elea</h1>
				<ul>
					<li class="type-categories grid-1-5">
						<a href="../img/location/beach1.jpg" data-lightbox="viandini" title="Viandini"><img src="../img/location/beach1.jpg" alt="Viandini - Beach next to Elea Mare Hotel"></a>
						<h3>Viandini</h3>
						<h4>50m from the hotel</h4>
					</li>
					<li class="type-categories grid-1-5">
						<a href="../img/location/beach2.jpg" data-lightbox="tigania" title="Tigania"><img src="../img/location/beach2.jpg" alt="Tigania - Beach close to Elea Mare Hotel"></a>
						<h3>Tigania</h3>
						<h4>700m from the hotel</h4>
					</li>
					<li class="type-categories grid-1-5">
						<a href="../img/location/beach3.jpg" data-lightbox="limani" title"Limani(Port)"><img src="../img/location/beach3.jpg" alt="Limani - Beach in Elaia village"></a>
						<h3>Limani (Port)</h3>
						<h4>400m from the hotel</h4>
					</li>
					<li class="type-categories grid-1-5">
						<a href="../img/location/beach4.jpg" data-lightbox="kavos" title"Kavos"><img src="../img/location/beach4.jpg" alt="Kavos - Beach next to Elia"></a>
						<h3>Kavos</h3>
						<h4>1.400m from the hotel</h4>
					</li>
				</ul>
			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer_en.php');?>

	</body>
</html>
