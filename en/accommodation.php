<?php

	$title = "Elea Mare Hotel:: accommodation";

	$discription = "Photos and details regarding the stay at Elea Mare Hotel.";

	include('header_en.php');

?>



	<body id="accommodation">



	<!-- Google Analytics -->



	<?php include_once("../analyticstracking.php") ?>



	<!-- MOBILE MENU - VIEPORT < 640PX -->



	<?php include('navigation_mobile_en.php');?>



  <header id="header">



		<!-- MAIN BACKGROUND -->



		<div id="slider" class="nivoSlider">

	    <img src="../img/main-backgrounds/accommodation.jpg" alt="Accomodation at Elea Mare Hotel" />

  	</div>



  	<!-- INCLUDE NAVIGATION & BOOK NOW -->



		<?php include_once('navigation_en.php');?>



	</header>



		<section id="main" class="container">



			<div class="mainbar grid-4">

				<div class="main-text">

					<h1>Accommodation - Book direct for Exclusive Offers</h1>

					<p>

						The hotel offers 22 studios. All of them are spacious with air-condition, Wi-Fi, kitchenette, fridge, TV, hairdryer, telephone and bathroom with shower.
						<br><span style="font-style:italic">* Pets are not allowed</span>
					</p>

					<p class="checkin">Check in: 14:00 – 21:00 / Check out: 7:00 – 12:00.</p>

				</div>

			</div>



			<div class="sidebar grid-2">

				<h1>Facilities</h1>

				<ul class="services">

					<li>Complimentary Wi-Fi</li>

					<li>Continental Breakfast (8:00 – 12:00)</li>

					<li>Parking</li>

					<li>Car rental</li>

				</ul>

			</div>



			<div class="categories grid-full">

				<h1>Rooms Types</h1>

				<p class="grid-full"> * Click on the photos to see more</p>

				<h2>Double</h2>

				<div class="type-categories grid-2">

					<a href="../img/accommodation/new/double-studio-partial-sea-view-1.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View"><img src="../img/accommodation/new/double-studio-partial-sea-view-1.jpg" alt="Δίdouble-studio-partial-sea-view"></a>
					<a href="../img/accommodation/new/double-studio-partial-sea-view-2.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View" class="hidden"></a>
					<a href="../img/accommodation/new/double-studio-partial-sea-view-3.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View" class="hidden"></a>
					<a href="../img/accommodation/new/double-studio-partial-sea-view-4.jpg" data-lightbox="double-studio-partial-sea-view-group" title="Double Studio with Partial Sea View" class="hidden"></a>

					<h3>Double Studio with Partial Sea View
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 2 Single Beds<br>
								- 35 m²<br>
								- Ground Floor
							</span>
						</span>
					</h3>

				</div>



					<!-- hidden images for lightbox group -->

					<div class="type-categories grid-2">
						<a href="../img/accommodation/new/double-seafront-1.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront"><img src="../img/accommodation/new/double-seafront-1.jpg" alt="double-seafront"></a>
						<a href="../img/accommodation/new/double-seafront-2.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront" class="hidden"></a>
						<a href="../img/accommodation/new/double-seafront-3.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront" class="hidden"></a>
						<a href="../img/accommodation/new/double-seafront-4.jpg" data-lightbox="double-seafront-group" title="Standard Double Studio Seafront" class="hidden"></a>
					<h3>Standard Double Studio Seafront
						<br>
						<span class="cat-info">
							<span class="cat-info-box">
								- 2 Single Beds<br>
								- 35 m²<br>
								- Ground or upper floor depending on availability
							</span>
						</span>
					</h3>

				</div>

				<div class="type-categories grid-2">
					<a href="../img/accommodation/new/superior-double-1.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront"><img src="../img/accommodation/new/superior-double-1.jpg" alt="superior-double"></a>
					<a href="../img/accommodation/new/superior-double-2.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront" class="hidden"></a>
					<a href="../img/accommodation/new/superior-double-3.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront" class="hidden"></a>
					<a href="../img/accommodation/new/superior-double-4.jpg" data-lightbox="superior-double-group" title="Superior Double Studio Seafront" class="hidden"></a>
				<h3>Superior Double Studio Seafront
					<br>
					<span class="cat-info">
						<span class="cat-info-box">
							- 1 King Size bed by Coco-Mat (2mΧ2m)<br>
							- 35 m²<br>
							- Upper Floor<br>
							- Laptop safe
						</span>
					</span>
				</h3>

			</div>

		</div>

		<div class="categories grid-full">

			<h2>Triple</h2>

			<div class="type-categories grid-2">
				<a href="../img/accommodation/new/triple-studio-1.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafront"><img src="../img/accommodation/new/triple-studio-1.jpg" alt="triple-studio"></a>
				<a href="../img/accommodation/new/triple-studio-2.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafront"></a>
				<a href="../img/accommodation/new/triple-studio-3.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafront" class="hidden"></a>
				<a href="../img/accommodation/new/triple-studio-4.jpg" data-lightbox="triple-studio-group" title="Standard Triple Studio Seafront" class="hidden"></a>
			<h3>Standard Triple Studio Seafront
				<br>
				<span class="cat-info">
					<span class="cat-info-box">
						- 3 Single Beds<br>
						- 35 m²<br>
						- Ground or upper floor depending on availability
					</span>
				</span>
			</h3>

		</div>

		<div class="type-categories grid-2">
			<a href="../img/accommodation/new/superior-triple-1.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront"><img src="../img/accommodation/new/superior-triple-1.jpg" alt="superior-triple"></a>
			<a href="../img/accommodation/new/superior-triple-2.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront" class="hidden"></a>
			<a href="../img/accommodation/new/superior-triple-3.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront" class="hidden"></a>
			<a href="../img/accommodation/new/superior-triple-4.jpg" data-lightbox="superior-triple-group" title="Superior Triple Studio Seafront" class="hidden"></a>
		<h3>Superior Triple Studio Seafront
			<br>
			<span class="cat-info">
				<span class="cat-info-box">
					- 1 King Size bed by Coco-Mat (2mΧ2m) + 1 Single Bed<br>
					- 40 m²<br>
					- Upper Floor<br>
					- Laptop safe
				</span>
			</span>
		</h3>

	</div>

</div>

<div class="categories grid-full">

	<h2>Quadruple</h2>


	<div class="type-categories grid-2">
		<a href="../img/accommodation/new/standard-two-bedroom-apartment-1.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront"><img src="../img/accommodation/new/standard-two-bedroom-apartment-1.jpg" alt="standard-two-bedroom-apartment"></a>
		<a href="../img/accommodation/new/standard-two-bedroom-apartment-2.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront" class="hidden"></a>
		<a href="../img/accommodation/new/standard-two-bedroom-apartment-3.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront" class="hidden"></a>
		<a href="../img/accommodation/new/standard-two-bedroom-apartment-4.jpg" data-lightbox="standard-two-bedroom-apartment-group" title="Standard Two-Bedroom Apartment Seafront" class="hidden"></a>
	<h3>Standard Two-Bedroom Apartment Seafront
		<br>
		<span class="cat-info">
			<span class="cat-info-box">
				- 4 Single Beds<br>
				- 50 m²<br>
				- Ground Floor
			</span>
		</span>
	</h3>

</div>

<div class="type-categories grid-2">
	<a href="../img/accommodation/new/superior-two-bedroom-apartment-1.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"><img src="../img/accommodation/new/superior-two-bedroom-apartment-1.jpg" alt="superior-two-bedroom-apartment"></a>
	<a href="../img/accommodation/new/superior-two-bedroom-apartment-2.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"></a>
	<a href="../img/accommodation/new/superior-two-bedroom-apartment-3.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"></a>
	<a href="../img/accommodation/new/superior-two-bedroom-apartment-4.jpg" data-lightbox="superior-two-bedroom-apartment-group" title="Superior Two-Bedroom Apartment Seafront"></a>
<h3>
Superior Two-Bedroom Apartment Seafront
	<br>
	<span class="cat-info">
		<span class="cat-info-box">
			- 1 King Size bed by Coco-Mat (2mΧ2m) + 2 Single Beds<br>
			- 50 m²<br>
			- Upper Floor<br>
			- Laptop safe
		</span>
	</span>
</h3>

</div>

</div>

<div class="categories grid-full">

	<h2>Acqua Suite</h2>

	<div class="type-categories grid-2">
		<a href="../img/accommodation/new/acqua-1.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)"><img src="../img/accommodation/new/acqua-1.jpg" alt="acqua"></a>
		<a href="../img/accommodation/new/acqua-2.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
		<a href="../img/accommodation/new/acqua-3.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
		<a href="../img/accommodation/new/acqua-4.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
		<a href="../img/accommodation/new/acqua-5.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
		<a href="../img/accommodation/new/acqua-6.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
		<a href="../img/accommodation/new/acqua-7.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
		<a href="../img/accommodation/new/acqua-8.jpg" data-lightbox="acqua-group" title="Acqua Suite (2 Double Connected Studios)" class="hidden"></a>
	<h3>
		Acqua Suite (2 Double Connected Studios)

		<br>
		<span class="cat-info">
			<span class="cat-info-box">
				- 1 King Size bed by Coco-Mat (2mΧ2m) + 2 Single Beds<br>
				- 60 m²<br>
				- Ground Floor<br>
				- 1 Laptop safe<br>
				- 2 TVs<br>
				- 2 Hairdryers<br>
				- 2 Bathrooms<br>
				- 1 Full-length Mirror
			</span>
		</span>
	</h3>

	</div>

		</section>



		<!-- INCLUDE FOOTER -->



		<?php include_once('footer_en.php');?>



	</body>

</html>
