<?php
	$title = "Elea Mare Hotel in Elea of Monemvasia, Lakonia";
	$discription = "Elea Mare Hotel is situated in Elea, a picturesque fishing village of Monemvasia. The location is ideal, with beautiful view to the sea, incredible sunset and easy access to the beach. The hotel stands 400 meters from the center of the village, in an advantageous position for visiting all the beautiful and historical places of Laconia.";
	include('header_en.php');
?>

	<body id="homepage">

	<!-- Google Analytics -->

	<?php include_once("../analyticstracking.php") ?>

	<!-- PRELOADER -->

	<div class="modal">
		<div class="pre-logo"></div>
	</div>

	<!-- MOBILE MENU - VIEPORT < 640PX -->

	<?php include('navigation_mobile_en.php');?>

	<header id="header">

		<!-- SLIDER -->

		<div id="slider" class="nivoSlider">
	    <img src="../img/slider/slider.jpg" alt="Elea Mare Hotel in Elea Monemvasia of Laconia" />
	    <img src="../img/slider/slider2.jpg" alt="Elea Mare Hotel in Elea Monemvasia of Laconia" />
	    <img src="../img/slider/slider3.jpg" alt="Family holidays in Elea Monemvasia of Lakonia" />
	    <img src="../img/slider/slider4.jpg" alt="Hotel in Elia Monemvasia of Lakonia" />
	    <img src="../img/slider/slider5.jpg" alt="Elea Mare Hotel in Peloponnese" />
	    <img src="../img/slider/slider6.jpg" alt="Hotel by the sea in Peloponnese" />
  	</div>

  <!-- INCLUDE NAVIGATION & BOOK NOW -->

	<?php include_once('navigation_en.php');?>

	</header>

	<section id="main" class="container">

		<div class="mainbar grid-5">

			<div class="main-text">
				<h1>Welcome to Elea Mare,</h1>
				<h2>in the heart of Laconia…</h2>
				<p>
					Elea Mare Hotel is situated in Elea, a picturesque fishing village of Monemvasia. The location is ideal, with beautiful view to the sea, incredible sunset and easy access to the beach.
				</p>
				<p>
					The hotel stands 400 meters from the center of the village, in an advantageous position for visiting all the beautiful and historical places of Laconia.
				</p>
				<p>
					Guests can explore the medieval castle of Monemvasia at 30 km from the hotel. Also, the Caves of Diros, the fortified town of Mystras and the famous Simos of Elafonisos are basic one-day excursions starting from Elea Mare.
				</p>
			</div>

			<div class="home-gallery">
				<ul>
					<li class="img-1"><img src="../img/img1.jpg" alt="Elea Mare Hotel by the sea" /></li>
					<li class="img-2"><img src="../img/img2.jpg" alt="Seaview balcony" /></li>
					<li class="img-3"><img src="../img/img3.jpg" alt="Rooms with view at the sea" /></li>
				</ul>
			</div>

		</div>

		<div class="sidebar grid-1">
			<div class="weather">
				<iframe scrolling="no" frameborder="0" allowtransparency="true" src="http://www.weather.gr/gr/gr/widgets/weather_w2.aspx?p=18036" style="width: 120px; height: 410px"></iframe><a target="blank" style="color: #999999; text-align: center; display: block; font: 10px/10px Arial,san-serif; text-decoration: none;" href="http://www.weather.gr">καιρός weather.gr</a>
			</div>
			<a href="elea_monemvasias_lakonias.php#formanchor" class="social-button">How to reach us</a>
		</div>

	</section>

	<!-- INCLUDE FOOTER -->

	<?php include_once('footer_en.php');?>

	</body>
</html>
