﻿<?php
	$title = "Elea Mare Hotel:: check reservation";
	$discription = "Check availability for Elea Mare Hotel.";
	include('header_en.php');
?>

	<body id="contact-form">

		<!-- Google Analytics -->
	
		<?php include_once("../analyticstracking.php") ?>

		<!-- MOBILE MENU - VIEPORT < 640PX -->

		<?php include('navigation_mobile_en.php');?>

		<header id="header">

			<!-- MAIN BACKGROUND -->

			<div id="slider" class="nivoSlider">
        <img src="../img/main-backgrounds/contact.jpg" alt="1" />
      </div>
			
			<!-- INCLUDE NAVIGATION & BOOK NOW -->
		
			<?php include_once('navigation_en.php');?>
     
		</header>

		<section id="main" class="container">
			
			<div class="mainbar grid-4">
				<div class="main-text">
					<a name="form2anchor" id="form2anchor"></a>
					<h1>Check availability - Book direct for Exclusive Offers</h1>
					<div class="contact-form grid-full">
						<?php
              if ($_GET['msg'] !="ok") {
            ?>

	            <form class="contact_form" method="post" action="check_en.php">
	                <div class="clear">
	                    <label for="checkinnew"> Check in * :</label>
	                    <input type="text" id="checkinnew" name="checkinnew" value="<?php echo htmlspecialchars($_POST['checkin']) ?>" required/>
	                </div>
	                <div class="clear">
	                    <label for="checkoutnew">  Check out * :</label>
	                    <input type="text" id="checkoutnew" name="checkoutnew" value="<?php echo htmlspecialchars($_POST['checkout']) ?>" required/>
	                </div>
	                <div class="clear">
	                    <label for="roomsnew"> Rooms * :</label>
	                    <input type="text" id="roomsnew" name="roomsnew" value="<?php echo $_POST['rooms']; ?>" onkeypress="return isNumber(event)" required/>
	                </div>
	                <div class="clear">
	                    <label for="peoplenew"> Persons * :</label>
	                    <input type="text" id="peoplenew" name="peoplenew" value="<?php echo $_POST['adults']; ?>" onkeypress="return isNumber(event)" required/>
	                </div>
	                <div class="clear">
	                	<label for="bedsnew" style="padding-top: 18px;"> Type of extra bed :</label>
		                <select name="bedsnew" id="bedsnew">
		       							<option value="0">None</option>
		       							<option value="1">Extra bed (3-16 years old)</option>
		        						<option value="2">Baby cot (1-2 years old)</option>
		        					</select>
		        				</div>
	                <div class="clear">
	                    <label for="name"> Name / Surname * :</label>
	                    <input type="text" id="name" name="name" placeholder="Name / Surname" required/>
	                </div>
	                <div class="clear">
	                    <label for="country">  City / Country:</label>
	                    <input type="text" id="country" name="country" placeholder="City / Country" />
	                </div>
	                <div class="clear">
	                    <label for="phone"> Telephone * :</label>
	                    <input type="text" id="phone" name="phone" placeholder="Telephone"  onkeypress="return isNumber(event)" required/>
	                </div>
	                 <div class="clear">
	                    <label for="email"> E-mail * :</label>
	                    <input type="email" id="email" name="email" placeholder="E-mail" required/>
	                </div>
	                <div class="clear">
	                    <label for="message">  Message * :</label>
	                    <textarea id="message" name="message" placeholder="Message" required></textarea>
	                </div>
	                <div class="clear">
	                    <button type="submit" class="button2" value="Send">
	                    <em>Send</em>
	                    </button>
	                    <input id="submitted" type="hidden" name="submitted" value="true" />
	                </div>
	            </form>

	            <?php
            		} else {
                	echo "<div id='sentmsg'>Your message was sent successfully. You will receive our offer as soon as possible to the e-mail you provided. Thank you very much.</div>";
            		}
              ?>
	        </div>
				</div>
			</div>

			<div class="sidebar grid-2">
				<h1>Contact Information</h1>
				<p>Elea Mare Hotel</p>
				<ul class="services">
					<li>Elea Monemvasia, Greece</li>
					<li>Postal Code: 23052</li>
					<li>Telephone: +30 27320 57540-1</li>
					<li>Fax: +30 27320 57551</li>
					<li>E-mail: eleamare@gmail.com</li>
				</ul>
				<p>Front Office / Call Center: 7:00 – 21:00</p>
			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer_en.php');?>
		
	</body>
</html>
