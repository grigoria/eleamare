<?php
	$title = "Ξενοδοχείο Elea Mare:: επικοινωνία";
	$discription = "Επικοινωνήστε μαζί μας. Ξενοδοχείο Elea Mare.";
	include('header.php');
?>

	<body id="contact">

		<!-- Google Analytics -->
	
		<?php include_once("analyticstracking.php") ?>

		<!-- MOBILE MENU - VIEPORT < 640PX -->

		<?php include('navigation_mobile.php');?>

		<header id="header">

			<!-- MAIN BACKGROUND -->

			<div id="slider" class="nivoSlider">
        <img src="img/main-backgrounds/contact.jpg" alt="Ξενοδοχείο στη θάλασσα" />
      </div>

      <!-- INCLUDE NAVIGATION & BOOK NOW -->
		
			<?php include_once('navigation.php');?>

		</header>

		<section id="main" class="container">

			<div class="mainbar grid-4">
				<div class="main-text">
					<a name="form1anchor" id="form1anchor"></a>
					<h1>Φόρμα επικοινωνίας - Book direct for Exclusive Offers</h1>
					<div class="contact-form grid-full">
            <?php
              if ($_GET['msg'] !="ok") {
            ?>
	            <form class="contact_form" method="post" action="mail-contact.php">
	                <div class="clear">
	                    <label for="name"> Όνομα / Επώνυμο * :</label>
	                    <input type="text" id="name" name="name" placeholder="Όνομα / Επώνυμο" required/>
	                </div>
	                <div class="clear">
	                    <label for="country">  Χώρα / Πόλη :</label>
	                    <input type="text" id="country" name="country" placeholder="Χώρα / Πόλη" />
	                </div>
	                <div class="clear">
	                    <label for="phone"> Τηλέφωνo :</label>
	                    <input type="text" id="phone" name="phone"  placeholder="Τηλέφωνο"  onkeypress="return isNumber(event)" />
	                </div>
	                <div class="clear">
	                    <label for="email"> E-mail * :</label>
	                    <input type="email" id="email" name="email"  placeholder="E-mail" required/>
	                </div>
	                <div class="clear">
	                    <label for="message">  Μήνυμα * :</label>
	                    <textarea id="message" name="message"  placeholder="Μήνυμα" required></textarea>
	                </div>
	                <div class="clear">
	                    <button type="submit" class="button2" value="αποστολή">
	                    <em>Αποστολή</em>
	                    </button>
	                    <input id="submitted" type="hidden" name="submitted" value="true" />
	                </div>
	            </form>
	            <?php
            		} else {
                	echo "<div id='sentmsg'>Το μήνυμά σας στάλθηκε με επιτυχία. Ευχαριστούμε πολύ.</div>";
            		}
              ?>
	        </div>
				</div>
			</div>

			<div class="sidebar grid-2">
				<h1>Στοιχεία επικοινωνίας</h1>
				<p>Ξενοδοχείο Elea Mare</p>
				<ul class="services">
					<li>Ελαία Μονεμβασίας, Λακωνία</li>
					<li>T.K.: 23052</li>
					<li>Tηλέφωνο: +30 27320 57540-1</li>
					<li>Fax: +30 27320 57551</li>
					<li>E-mail: eleamare@gmail.com</li>
				</ul>
				<p>Υποδοχή – Τηλεφωνικό Κέντρο: 7:00 – 21:00</p>
				<a href="kratiseis.php#form2anchor" class="social-button">ΕΛΕΓΧΟΣ ΔΙΑΘΕΣΙΜΟΤΗΤΑΣ</a>
			</div>

		</section>

		<!-- INCLUDE FOOTER -->

		<?php include_once('footer.php');?>
		
	</body>
</html>
